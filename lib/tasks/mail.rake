desc "Task for send mail"
namespace :mail do

  desc "Send Mail Review"
  task review: :environment do
  	puts "Finding email for sending."
  	checkouts = Checkout.where(:check_in => Date.today, :review => false, :status => %w(Confirmed Changed))
    unless checkouts.empty?
      checkouts.each do |checkout|
        ContactUs.send_review(checkout).deliver
        checkout.review = true
        checkout.save
    		puts "Email sent to #{checkout.email_id} at #{Time.now}."
      end
    end
  end

end
