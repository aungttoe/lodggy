namespace :facilities do
  task create: :environment do
    puts "Deleting old facilities."
    Facility.destroy_all
    Property.all.each do |property|
      property.facilities.destroy_all
    end

    Room.all.each do |room|
      room.facilities.destroy_all
    end
    puts "Old facilities deleted."
    puts "Create Facilities."
    facilities_array = [
      ['Bicycle Hire', 'fa-bicycle'],
      ['Elevator', 'fa-elevator'],
      ['First Aid Kit', 'fa-medkit'],
      ['Free Parking', 'fa-parking'],
      ['Gym', 'fa-gym'],
      ['Free Internet', 'fa-wifi'],
      ['Restaurant', 'fa-cutlery'],
      ['Laundry Service', 'fa-laundry'],
      ['Swimming Pool', 'fa-pool'],
      ['ATM', 'fa-atm'],
      ['Suitable for Events', 'fa-events'],
      ['Family Friendly', 'fa-users'],
      ['Pets Allowed', 'fa-pets'],
      ['Wheel Chair Accessible', 'fa-wheelchair'],
      ['24 Hour Reception', 'fa-reception'],
      ['Fire Extinguisher', 'fa-fire-extinguisher'],
      ['Tour Desk', 'fa-tour-desk'],
      ['Shampoo', 'fa-shampoo'],
      ['Bathroom Essentials', 'fa-bathroom'],
      ['Hair Dryer', 'fa-hair-dryer'],
      ['Water Heater', 'fa-water-heater'],
      ['Air Conditioning', 'fa-air-conditioning'],
      ['TV', 'fa-tv'],
      ['Adaptors', 'fa-adaptors'],
      ['Reading Lights', 'fa-reading'],
      ['Keycard Access', 'fa-key-card'],
      ['Phone', 'fa-phone'],
      ['Smoke Detector', 'fa-smoke-detector'],
      ['Tea and Coffee', 'fa-coffee'],
      ['Mini Fridge', 'fa-mini-fridge']
    ]
    facilities_array.each do |facility|
      Facility.create(:facility_name => facility[0], :facility_type => 'Facility', :icon_name => facility[1])
    end

    payment_array = [['Cash', 'fa-paypal'], ['Credit/Debit Card', 'fa-credit-card']]

    payment_array.each do |payment|
      Facility.create(:facility_name => payment[0], :facility_type => "Payment", :icon_name => payment[1])
    end
    puts "Facilities Created."
  end
end
