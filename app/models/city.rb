class City < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :user
  has_many :places, :dependent => :destroy
  has_many :rooms, :through => :properties
  has_many :properties, :dependent => :destroy

  mount_uploader :cityimg
  
  serialize :coordinates, Array

  def full_address
    "#{self.name}"
  end

  def look_up_address
   # self.coordinates = [self.longitude, self.latitude]
    self.lng = self.coordinates[0]
    self.lat = self.coordinates[1]
    reverse_geocode
  end



  # after_validation :add_low_price

  def add_low_price
    if self.hotels.count >= 1
      @hotels = self.hotels
      @hotel = @hotels.order_by([:private_room_price, :asc])
      @hotel_min = @hotel.first
      self.min_price = @hotel_min.private_room_price
    else
      self.min_price = 0
    end
  end

    
end
