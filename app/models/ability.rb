class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    
    if user.roles? :admin
      can :manage, :all
    elsif user.roles? :hotel
      can :manage, City
      can :manage, Property
      can :manage, Photo
      can :manage, Room
      can :manage, RoomDate
      can :manage, Comment
      can :manage, Checkout
      can :manage, UserRefund
      can :manage, BlogComment
      can [:read, :tags], Blog
      can :read, Page
      can :read, Faq
    else
      can [:read, :tags], Blog
      can :manage, BlogComment
      can :manage,  Checkout
      can :manage, Checkout
      can :manage, UserRefund
      can [:read, :map, :cities_info], City
      can :create, Comment
      can :read, Faq
      can :read, Place
      can :read, Room
      can :read, Page
    end
  end
end