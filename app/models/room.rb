class Room < ActiveRecord::Base
  after_create :price_property
  after_create :guest_property
  
  mount_uploader :photo, ImageUploader

  belongs_to :room_type
  belongs_to :property
  has_one :city, :through => :property
  has_many :checkouts, :dependent => :destroy
  has_many :room_dates, :dependent => :destroy
  has_and_belongs_to_many :facilities
  
  accepts_nested_attributes_for :facilities

  #scope
  scope :available, -> {where("count > 0")}


  def cancellation_policy
    if self.cancellation.eql?("Flexible")
      "Can cancel up to 3 day Prior Check In"
    elsif self.cancellation.eql?("Moderate")
      "Can cancel up to 7 day Prior Check In"
    elsif self.cancellation.eql?("Strict")
      "Can cancel up to 14 day Prior Check In"
    end
  end

  def price_property
    property = self.property
    if property.rooms.blank?
      property.price = self.price
    else
      room = property.rooms.order(:price => :asc).first
      property.price = room.price
    end

    property.save
  end
  
  def guest_property
    property = self.property
    if property.rooms.blank?
      property.guest = selft.guest
    else
      room = property.rooms.order(:price => :desc).first
      property.guests = room.guest
    end
  end

  def self.filter_check(check_in, check_out, guest,  min, max)
    if check_in && check_out
      check_in = check_in.to_date
      check_out = check_out.to_date
      
      room = Room.where("check_in_room >= ? AND check_out_room <= ?", check_in, check_out)

      if guest
        room = room.where("guest <= ?", guest.to_i)
      end

      if min && max
        room = room.where("price >= ? AND price <= ? ", min.to_i, max.to_i)
      end
    end
    room
  end
end


