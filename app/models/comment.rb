class Comment < ActiveRecord::Base
  belongs_to :property, :counter_cache => true
  belongs_to :user
  belongs_to :checkout

  def make_general_rating
    self.general_rating = (self.atmosphere + self.staff + self.location + self.value_for_money + self.security + self.cleanliness + self.facilities) / 7 
  end

end
