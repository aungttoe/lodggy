class Property < ActiveRecord::Base
	extend FriendlyId  
  friendly_id :title, use: :slugged

	has_many :photos, dependent: :destroy
	has_many :comments, dependent: :destroy
	has_many :checkouts, through: :rooms
	has_many :rooms, dependent: :destroy
	belongs_to :city
	belongs_to :user
	has_and_belongs_to_many :facilities
	
	accepts_nested_attributes_for :facilities

	# scope
	scope :published, -> {where(:published => true)}
	scope :prices, lambda { |min, max| where("price >= ? AND price <= ?", min, max) }
	scope :ratings, lambda {|from, to| where("rating >= ? AND rating <= ?", from, to) }
	scope :with_user, lambda {|user_id| where(:user_id => user_id)}

	def make_rating
		atmosphere 					= self.comments.pluck(:atmosphere).sum
		staff 		 					= self.comments.pluck(:staff).sum
		location 	 					= self.comments.pluck(:location).sum
		value_for_money 	  = self.comments.pluck(:value_for_money).sum
		security 	 					= self.comments.pluck(:security).sum
		cleanliness 	 			= self.comments.pluck(:cleanliness).sum
		facilities 	 				= self.comments.pluck(:facilities).sum

    self.atmosphere_rating 				 = atmosphere				/ self.comments.length
    self.staff_rating 					   = staff						/ self.comments.length
    self.location_rating 					 = location					/ self.comments.length
    self.value_of_money_rating 	   = value_for_money	/ self.comments.length
    self.security_rating 					 = security					/ self.comments.length
    self.cleanliness_rating 			 = cleanliness			/ self.comments.length
    self.facilities_rating 				 = facilities				/ self.comments.length
    
    self.save
	end

	def add_price
    rooms = self.rooms
    rooms = rooms.order(:price => :asc)
    room_min = rooms.first
    if rooms.count > 0
      self.price = room_min.price
      self.save
    end
  end

  def photo
  	photos.first.image
  end

  def photo_thumb
    photos.first.image_url(:thumb)
  end

  def self.search(query)
    where("title ILIKE :query", :query => "%#{query}%").order(:id => :desc)
  end

  def cancellation_policy
    if self.cancellation.eql?("Flexible")
      3
    elsif self.cancellation.eql?("Moderate")
      7
    elsif self.cancellation.eql?("Strict")
      14
    end
  end
end
