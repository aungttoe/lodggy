class Blog  < ActiveRecord::Base
  extend FriendlyId
  acts_as_taggable
  acts_as_taggable_on :tags
  
  friendly_id :title, use: :slugged
  
  mount_uploader :image, ImageUploader

	has_many :blog_comments, :dependent => :destroy
	belongs_to :blog_category
end
