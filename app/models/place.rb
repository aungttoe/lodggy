class Place < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  mount_uploader :placeimg, PlaceimgUploader

  belongs_to :city
  
end
