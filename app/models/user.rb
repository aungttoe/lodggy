class User < ActiveRecord::Base
  include PublicActivity::Common
  before_validation :set_role

  mount_uploader :avatar, AvatarUploader

  serialize :coordinates, Array
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :omniauthable,
         :confirmable
         
  has_many :properties, :dependent => :destroy
  has_many :checkouts, :dependent => :destroy
  has_one :user_refunds, :through => :checkouts

  validates :role, :email, :presence => true

  def roles?(role)
    self.role == role.to_s
  end

  def self.from_omniauth(auth)
    if auth.provider == "google_oauth2"
      where(auth.slice(:provider, :uid)).first_or_create do |user|
        user.provider    = auth.provider
        user.uid         = auth.uid
        user.first_name  = auth.info.first_name
        user.last_name   = auth.info.last_name
        user.email       = auth.info.email
      end
    elsif auth.provider == "facebook"
      where(auth.slice(:provider, :uid)).first_or_create do |user|
        user.provider    = auth.provider
        user.uid         = auth.uid
        user.first_name  = auth.extra.raw_info.first_name
        user.last_name   = auth.extra.raw_info.last_name
        user.email       = auth.extra.raw_info.email
        user.gender      = auth.extra.raw_info.gender.capitalize
        user.birth       = auth.extra.raw_info.birthday
      end
    end
  end

  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted_password.blank?
      update_attributes(params, *options)
    else
      super
    end
  end

  def set_role
    if self.role.nil?
      self.role = "user"
    end
  end

  def self.search(query)
    where("email ILIKE :query OR first_name ILIKE :query OR last_name ILIKE :query", :query => "%#{query}%").order(:id => :desc)
  end
end
