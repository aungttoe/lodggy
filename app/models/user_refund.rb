class UserRefund < ActiveRecord::Base
	include PublicActivity::Common
	belongs_to :checkout
	has_one :property, :through => :checkout
	has_one :room, :through => :checkout
end
