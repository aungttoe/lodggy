class FaqPost  < ActiveRecord::Base

  belongs_to :faq_category, class_name: 'FaqCategory', inverse_of: :faq_posts

end

