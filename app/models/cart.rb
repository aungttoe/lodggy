class Cart < ActiveRecord::Base
	acts_as_shopping_cart_using :cart_item

	def government_tax
    subtotal - (subtotal * 0.9)
	end
	def service_tax
    subtotal - (subtotal * 0.95)
	end

	def taxes
    government_tax + service_tax
  end
end
