class Checkout < ActiveRecord::Base
  include PublicActivity::Common

  after_create :set_reference_id, :set_booking_id, :create_room_dates
  after_update :create_room_dates
  paginates_per 20
  
  belongs_to :user
  has_one :user_refund, :dependent => :destroy
  belongs_to :room
  has_one :property, :through => :room
  has_one :room_date, :dependent => :destroy
  has_one :comment, :dependent => :destroy
  
  def filter_edit_checkout_date(checkout, check_in, check_out)

    @checkout = checkout   
    @check_in = check_in 
    @check_out = check_out
    @checkout.type.each do |type|
      @room = Room.where(type: type).first
      if @room.check_in_room < @check_in
        @update = false
      elsif @room.check_out_room > @check_out
        @update = false
      else
        @update = true
      end
      if @update == false
        break
      end
    end  
   
    if @update == false
      false
    else
      true
    end
    
  end

  def set_reference_id
    self.reference_id = SecureRandom.base64(8).tr("+/=", "Zab")
    self.save
  end

  def set_booking_id
    self.booking_id = SecureRandom.hex(4).tr("abcdef", rand(1..9).to_s).to_i
    self.save
  end

  def create_room_dates
    if self.status.eql?("Confirmed")
      dates             = RoomDate.new
      dates.from        = self.check_in
      dates.to          = (self.check_out + 1.day)
      dates.room_id     = self.room_id
      dates.type_date   = "booked"
      dates.no_room     = self.no_room 
      dates.checkout_id = self.id
      dates.save
    end

    if self.status.eql?("Changed")
      dates         = self.room_date
      dates.from    = self.check_in
      dates.to      = (self.check_out + 1.day)
      dates.no_room = self.no_room
      dates.save
    end
  end

  def cancellation
    self.room.cancellation
  end

  def flexible?
    self.room.cancellation.eql?("Flexible")
  end


  def strict?
    self.room.cancellation.eql?("Strict")
  end

  def moderate?
    self.room.cancellation.eql?("Moderate")
  end

  def cancellation_policy
    if self.room.cancellation.eql?("Flexible")
      3
    elsif self.room.cancellation.eql?("Moderate")
      7
    elsif self.room.cancellation.eql?("Strict")
      14
    end
  end

  def free_change?
    now = Date.today
    if self.flexible?
      free = now < (self.check_in - 3.day)
    end

    if self.moderate?
      free = now < (self.check_in - 7.day)
    end

    if self.strict?
      free = now < (self.check_in - 14.day)
    end
    free
  end

  def self.search(query)
    where("email_id ILIKE :query OR first_name ILIKE :query OR last_name ILIKE :query OR booking_id = :number", :query => "%#{query}%", :number => query.to_i).order(:id => :desc)
  end
end