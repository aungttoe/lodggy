class RoomDate < ActiveRecord::Base
	
	belongs_to :room
	belongs_to :checkout
	has_one :property, :through => :room

	scope :available, -> {where(:type_date => 'available')}
	scope :available_date, -> {where("room_dates.to >= ?", Date.today)}
end
