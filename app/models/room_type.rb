class RoomType < ActiveRecord::Base
  has_many :rooms, class_name: 'Room', inverse_of: :room_type, :dependent => :destroy
end
