class Hotel < ActiveRecord::Base
  validates :address, :address_two, :state, :title, :property_type, :total_number, :number_of_beds, :private_room_price,
    :contact_name, :manager_email, :bookings_email, :currency, :language, :phone_number,
    :content, :description, :location, :directions, :things, presence: true
 
  belongs_to :city
  belongs_to :user
  has_many :comments, :dependent => :destroy
  has_many :checkouts, :dependent => :destroy
  has_many :rooms, :dependent => :destroy

 #  #CARRIEWAVE
  has_many :photos, :inverse_of => :hotel, :dependent => :destroy
  accepts_nested_attributes_for :photos, allow_destroy: true

  after_destroy :delete_images!

  def delete_images!
    self.photos.each do |image|
      image.remove_image!
    end
  end

  
  def add_price

      @rooms = self.rooms
      #@hotels = self.hotels
      @rooms = @rooms.order(:price => :asc)
      #@hotel = @hotels.order_by([:private_room_price, :asc])
      @room_min = @rooms.first
      #@hotel_min = @hotel.first
      if @rooms.count > 0
        self.private_room_price = @room_min.price
        self.save
      end
  end

  def make_hotels_general_rating
    self.general_rating = 0
    if self.comments.length > 0
      self.comments.each do |comment|
        self.general_rating = self.general_rating + comment.general_rating
      end
      self.general_rating = self.general_rating/self.comments.length
    end
    self.save
  end

  def make_rating_params
      self.atmosphere_percent = 0
      self.staff_percent = 0
      self.location_percent = 0
      self.value_for_money_percent = 0
      self.security_percent = 0
      self.cleanliness_percent = 0
      self.facilities_percent = 0
    if self.comments.length > 0
      self.comments.each do |comment|
        self.atmosphere_percent = self.atmosphere_percent + comment.atmosphere_percent
        self.staff_percent = self.staff_percent + comment.staff_percent
        self.location_percent = self.location_percent + comment.location_percent
        self.value_for_money_percent = self.value_for_money_percent + comment.value_for_money_percent
        self.security_percent = self.security_percent + comment.security_percent
        self.cleanliness_percent = self.cleanliness_percent + comment.cleanliness_percent
        self.facilities_percent = self.facilities_percent + comment.facilities_percent
        self.save
      end
      self.atmosphere_percent = self.atmosphere_percent/self.comments.length
      self.staff_percent = self.staff_percent/self.comments.length
      self.location_percent = self.location_percent/self.comments.length
      self.value_for_money_percent = self.value_for_money_percent/self.comments.length
      self.security_percent = self.security_percent/self.comments.length
      self.cleanliness_percent = self.cleanliness_percent/self.comments.length
      self.facilities_percent = self.facilities_percent/self.comments.length
      self.save
    end
  end

  def filter_check_by_date(hotel_id, check_in, check_out)
    if check_in.empty? && check_out.empty?
      Room.where('hotel_id' => hotel_id)
    elsif check_in && check_out.empty?
      Room.where('hotel_id' => hotel_id).where("check_in_room >= ?", check_in.to_date)
    elsif check_in.empty? && check_out
      Room.where('hotel_id' => hotel_id).where("check_out_room <= ?", check_out.to_date)
    else
      Room.where("hotel_id = ? AND check_in_room >= ? AND check_out_room <= ?", hotel_id, check_in.to_date, check_out.to_date)
    end
  end

  def percent_rating
  	self.rating_percent = self.rating*20
  end

  def self.filter(from, to, city, all_property, hostels, bed, hotels, ensuite_room, single_room, twin_room, double_room, triple_room, family_room, mixed_dorm, male_dorm, female_dorm, breakfast_included, internet_access, luggage_storage, hour_check_in, restaurant, bar, air_conditioning, common_room, travel_desk, wheelchair_friendly,  all_payment, credit_card, debit_card, paypal, min, max)

    @min = min 
    @max = max

    @from = from
    @to = to.to_i+1;
    @city = city
    @all_property = all_property
    @hostels = hostels
    @bed = bed
    @hotels = hotels

    @ensuite_room = ensuite_room
    @single_room = single_room
    @twin_room = twin_room
    @double_room = double_room
    @triple_room = triple_room
    @family_room = family_room

    @mixed_dorm = mixed_dorm
    @male_dorm = male_dorm
    @female_dorm = female_dorm

    @breakfast_included = breakfast_included
    @internet_access = internet_access
    @luggage_storage = luggage_storage
    @hour_check_in = hour_check_in
    @restaurant = restaurant
    @bar = bar
    @air_conditioning = air_conditioning
    @common_room = common_room
    @travel_desk = travel_desk
    @wheelchair_friendly = wheelchair_friendly

    @all_payment = all_payment
    @credit_card = credit_card
    @debit_card = debit_card
    @paypal = paypal


    if @from.empty?
      @from = '0'
    end

    #.where('private_room_price'=> { '$gt' => @min  }).where('private_room_price' => {'$lt' => @max })

    Hotel.where('private_room_price'=> { '$gte' => @min  }).where('private_room_price' => {'$lte' => @max }).where('general_rating'=> { '$gte' => @from  }).where('general_rating' => {'$lte' => @to }).where('city_id' => @city).or('all_property' => @all_property).or('hostels' => @hostels).or('hostels' => @hostels).or('bed_and_breakfasts' => @bed).or('hotels' => @hotels).or('ensuite_room' => @ensuite_room).or('single_room' => @single_room).or('twin_room' => @twin_room).or('double_room' => @double_room).or('triple_room' => @triple_room).or('family_room' => @family_room).or('mixed_dorm' => @mixed_dorm).or('male_dorm' => @male_dorm).or('female_dorm' => @female_dorm).or('breakfast_included' => @breakfast_included).or('internet_access' => @internet_access).or('luggage_storage' => @luggage_storage).or('hour_check_in' => @hour_check_in).or('restaurant' => @restaurant).or('bar' => @bar).or('air_conditioning' => @air_conditioning).or('common_room' => @common_room).or('travel_desk' => @travel_desk).or('wheelchair_friendly' => @wheelchair_friendly).or('all_payment' => @all_payment).or('credit_card' => @credit_card).or('debit_card' => @debit_card).or('paypal' => @paypal)
  end

  def self.analog_filter(from, to, city,  min, max, title, *parameters)

    properties = ['all_property', 'hostels', 'bed', 'hotels', 'ensuite_room', 'single_room', 'twin_room', 'double_room', 'triple_room', 'family_room', 'mixed_dorm', 'male_dorm', 'female_dorm', 'breakfast_included', 'internet_access', 'luggage_storage', 'hour_check_in', 'restaurant', 'bar', 'air_conditioning', 'common_room', 'travel_desk', 'wheelchair_friendly',  'all_payment', 'credit_card', 'debit_card', 'paypal']
    from = from.to_i
    to = to.to_i+1
    min = min.to_i
    max = max.to_i
    city = city.to_i
    #@city = city

    parameters.map! { |parameter|
      parameter.to_i
    }
    properties.map!{|property|
      property.to_sym
    }

    filter = []
    i=0
    properties.each do |property|
      filter.push(property) if parameters[i] == 1
      i+=1
    end
    @hotels = self.where("published = true AND private_room_price >= ? AND private_room_price <= ? AND general_rating >= ? AND general_rating <= ? AND title ILIKE ? AND city_id = ?", min, max, from, to, "%#{title}%", city)
    filter.each do |f|
      @hotels = @hotels.where(f => true)
    end
    @hotels
  end

  def self.analog_filter_1(from, to, city,  min, max, *parameters)

    properties = ['all_property', 'hostels', 'bed', 'hotels', 'ensuite_room', 'single_room', 'twin_room', 'double_room', 'triple_room', 'family_room', 'mixed_dorm', 'male_dorm', 'female_dorm', 'breakfast_included', 'internet_access', 'luggage_storage', 'hour_check_in', 'restaurant', 'bar', 'air_conditioning', 'common_room', 'travel_desk', 'wheelchair_friendly',  'all_payment', 'credit_card', 'debit_card', 'paypal']
    @min = min 
    @max = max

    @from = from
    @to = to.to_i+1;
    #@city = city

    parameters.map! { |parameter|
      parameter.to_i
    }
    properties.map!{|property|
      property.to_sym
    }

    filter = []
    i=0
    properties.each do |property|
      filter.push(property) if parameters[i] == 1
      i+=1
    end

    @hotels = self.where('private_room_price'=> { '$gte' => @min  }).where('private_room_price' => {'$lte' => @max }).where('general_rating'=> { '$gte' => @from  }).where('general_rating' => {'$lte' => @to })
   
    filter.each do |f|
      @hotels = @hotels.where(f => true)
    end
    @hotels

  end

  def self.sort_by_rating(sort)
    if self
      if sort == 'asc'
         self.asc(:general_rating)
       elsif sort == 'desc'
         self.desc(:general_rating)
       elsif sort == 'none'
           self
       end
   end
 end

   def self.sort_by_price(price)
    if self
      if price == 'asc'
         self.asc(:private_room_price)
       elsif price == 'desc'
         self.desc(:private_room_price)
       elsif price == 'none'
         self
       end
     end
   end

  def show_room_types
     @types = []
     @types.push('Ensuite Room') if self.ensuite_room
     @types.push('Single Room') if self.single_room
     @types.push('Twin Room') if self.twin_room
     @types.push('Double Room') if self.double_room
     @types.push('Triple Room') if self.triple_room
     @types.push('Family Room') if self.family_room
     @types
  end
end
