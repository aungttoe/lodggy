class FaqPostsController < ApplicationController
  before_action :set_faq_post, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /faq_posts
  # GET /faq_posts.json
  def index
    @faq_posts = FaqPost.all
  end

  # GET /faq_posts/1
  # GET /faq_posts/1.json
  def show
  end

  # GET /faq_posts/new
  def new
    @faq_post = FaqPost.new
  end

  # GET /faq_posts/1/edit
  def edit
  end

  # POST /faq_posts
  # POST /faq_posts.json
  def create
    @faq_post = FaqPost.new(faq_post_params)

    respond_to do |format|
      if @faq_post.save
        format.html { redirect_to @faq_post, notice: 'Faq post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @faq_post }
      else
        format.html { render action: 'new' }
        format.json { render json: @faq_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /faq_posts/1
  # PATCH/PUT /faq_posts/1.json
  def update
    respond_to do |format|
      if @faq_post.update(faq_post_params)
        format.html { redirect_to @faq_post, notice: 'Faq post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @faq_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /faq_posts/1
  # DELETE /faq_posts/1.json
  def destroy
    @faq_post.destroy
    respond_to do |format|
      format.html { redirect_to faq_posts_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_faq_post
      @faq_post = FaqPost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def faq_post_params
      params.require(:faq_post).permit(:title, :content, :faq_category_id)
    end

    def my_sanitizer
      params.require(:faq_post).permit(:name)
    end
end
