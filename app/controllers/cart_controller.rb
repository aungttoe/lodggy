class CartController < ApplicationController
	
	def index
		if session[:cart_id] && session[:cart]
			room_ids = session[:cart].map {|a| a[:room_id].to_i}
			@rooms = Room.where("id IN (?)", room_ids)
			@cart = Cart.find(session[:cart_id])
		end
	end

	def create
		if session[:cart_id].nil?
			@cart = Cart.create
			session[:cart_id] = @cart.id
		else
			@cart = Cart.find(session[:cart_id])
			session[:cart] = nil
			@cart.clear
		end
		qty = params[:cart][:no_room].to_i
		room = Room.find(params[:cart][:room_id].to_i)
		price = params[:cart][:night].to_i * room.price
		@cart.add(room, price, qty)

		if session[:cart]
			exist = session[:cart].find_index{|item| item[:room_id].eql?(params[:cart][:room_id]) }
			if exist.nil?
				session[:cart].push(params[:cart])
			else
				session[:cart][exist][:no_room] = session[:cart][exist][:no_room].to_i + params[:cart][:no_room].to_i
			end

		else
			session[:cart] = [params[:cart]]
		end

		respond_to do |format|
			format.js
			format.html {redirect_to new_checkout_url}
		end
	end

	def destroy
		cart = Cart.find(session[:cart_id])
		cart.clear
		session[:cart_id] = nil
		session[:cart] = nil
		redirect_to cart_index_url
	end
end
