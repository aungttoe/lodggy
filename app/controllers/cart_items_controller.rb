class CartItemsController < ApplicationController
	def destroy
		room = Room.find(params[:id])
		cart = Cart.find(session[:cart_id])

		index = session[:cart].find_index {|item| item[:room_id].to_i.eql?(room.id) }
		session[:cart].delete_at(index)
		
		cart.remove(room)

		redirect_to cart_index_url
	end
end
