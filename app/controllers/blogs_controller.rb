class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer, :except => :comment

  # GET /blogs
  # GET /blogs.json
  def index
    if params[:title]
      @blogs = Blog.where("title ILIKE ?", "%#{params[:title]}%")
    else
      @blogs = Blog.order(:created_at => :desc)
    end
    @tags = ActsAsTaggableOn::Tag.most_used
    @popular_blogs = Blog.order(:created_at => :desc).limit(6)
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    #@blog.rating
    @blog_comments = @blog.blog_comments
  end

  def tags
    @blogs = Blog.tagged_with(params[:tag])
    @tags = ActsAsTaggableOn::Tag.most_used
    @popular_blogs = Blog.order(:created_at => :desc).limit(6)
    render "index"
  end

  private
    def set_blog
      @blog = Blog.friendly.find(params[:id])
    end

    def my_sanitizer
      params.require(:blog).permit(:name)
    end
end