class SearchController < ApplicationController
  before_action :set_properties

  def index
    if @title.nil?
      @title = {:title => "Search Lodgings", :description => "search lodgings in mmlodging", :keywords => %w(search lodging mmlodging hotel room booking city)}
    end
    @property_types = [["Hostel"], ["Bed and Breakfast"], ["Hotel"], ["Inn"], ["Motel"]]
    @facilities = Facility.where(:facility_type => 'Facility')
    @payments = Facility.where(:facility_type => 'Payment')
    @cities = City.all.map {|city| [city.name, city.slug]}
    @guests = Array(1..15)

    if params[:sort_by_rating] || params[:sort_by_price]
      if params[:sort_by_rating].eql?("desc")
        @properties = @properties.order(:rating => :desc)
      else
        @properties = @properties.order(:rating => :asc)
      end

      if params[:sort_by_price].eql?("desc")
        @properties = @properties.order(:price => :desc)
      else
        @properties = @properties.order(:price => :asc)
      end
    else
      @properties = @properties.order(:created_at => :desc)
    end

    @properties_map = @properties
    @properties = Kaminari.paginate_array(@properties).page(params[:page]).per(5)
  end

  def map_search
    @hash = []
    @properties.map {|p| @hash << {:latlng => [p.lat, p.lng]} }
    @panTo = []
    @properties.map {|p| @panTo << [p.lat, p.lng]}
  end

  private

  def set_properties
    @properties = Property.published.includes(:facilities, :photos)

    min = params[:minimum]
    max = params[:maximum]
    guest = params[:guest]
    from = params[:from]
    to = params[:to]
    city = City.friendly.find(params[:city]) rescue []
    title = params[:title]

    property_type = params[:property_type]
    room_type = params[:room_type]
    facility_type = params[:facility_type]
    payment_type = params[:payment_type]

    sort_by_price = params[:sort_by_rating]
    sort_by_rating = params[:sort_by_price]

    if min && max
      @properties = @properties.prices(min.to_i, max.to_i)
    end

    if guest
      @properties = @properties.where("guests >= ?", guest.to_i)
    end

    if city && !city.blank?
      @title = {:title => "Lodgings in #{city.name}", :description => city.description, :keywords => city.name.split( ) << ["search", "lodgings"]}
      @properties = @properties.where(:city_id => city.id)
    end

    if to && from
      @properties = @properties.ratings(from.to_i, to.to_i)
    end

    if property_type
      @properties = @properties.where("property_type IN(?)", property_type)
    end

    if room_type
      @properties = @properties.where("room_type IN(?)", room_type)
    end
    
    if facility_type
      @properties = @properties.where(:facilities => {:id => facility_type})
    end

    if payment_type
      @properties = @properties.where(:facilities => {:id => payment_type})
    end

    if title
      @properties = @properties.where("title ILIKE ?", "%#{title}%")
    end
  end
end
