class Admin::HomeimgsController < AdminController
  before_action :set_homeimg, only: [:show, :edit, :update, :destroy, :active]

  # GET /admin/homeimgs
  # GET /admin/homeimgs.json
  def index
    @homeimgs = Homeimg.order(:created_at => :desc)
  end

  # GET /admin/homeimgs/1
  # GET /admin/homeimgs/1.json
  def show
  end

  # GET /admin/homeimgs/new
  def new
    @homeimg = Homeimg.new
  end

  # GET /admin/homeimgs/1/edit
  def edit
  end

  # POST /admin/homeimgs
  # POST /admin/homeimgs.json
  def create
    @homeimg = Homeimg.new(homeimg_params)
    if homeimg_params[:active].eql?("true")
      homeimg = Homeimg.find_by(:active => true)
      homeimg.update(:active => false) unless homeimg.nil?
    end
    respond_to do |format|
      if @homeimg.save
        format.html { redirect_to admin_homeimgs_url, notice: 'Homeimg was successfully created.' }
        format.json { render action: 'show', status: :created, location: @homeimg }
      else
        format.html { render action: 'new' }
        format.json { render json: @homeimg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/homeimgs/1
  # PATCH/PUT /admin/homeimgs/1.json
  def update
    homeimg = Homeimg.find_by(:active => true)
    if homeimg_params[:active].eql?("true")
      homeimg = Homeimg.find_by(:active => true)
      homeimg.update(:active => false) unless homeimg.nil?
    end

    respond_to do |format|
      if @homeimg.update(homeimg_params)
        format.html { redirect_to admin_homeimgs_url, notice: 'Homeimg was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @homeimg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/homeimgs/1
  # DELETE /admin/homeimgs/1.json
  def destroy
    @homeimg.destroy
    respond_to do |format|
      format.html { redirect_to admin_homeimgs_url }
      format.json { head :no_content }
    end
  end

  def active

    if @homeimg.active?
      @homeimg.update(:active => false)
    else
      homeimg = Homeimg.find_by(:active => true)
      homeimg.update(:active => false) unless homeimg.nil?

      @homeimg.update(:active => true)
    end
    respond_to do |format|
      format.html { redirect_to admin_homeimgs_url}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homeimg
      @homeimg = Homeimg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homeimg_params
      params.require(:homeimg).permit(:title, :active, :image)
    end
end
