class Admin::UserRefundsController < AdminController
  def index
    if current_user.role.eql?("hotel")
      @user_refunds = UserRefund.joins(:property).where("properties.user_id = ?", current_user.id)
    else
  	  @user_refunds = UserRefund.all.order(:created_at => :desc)
    end
    @user_refunds = @user_refunds.page(params[:page])
  end

  def confirm_refund
  	checkout = Checkout.find(params[:checkout_id])
  	amount = params[:amount].to_f * 100
    
    if checkout.express_token.nil?
      response = STANDARD_GATEWAY.refund(amount, checkout.transaction_id)
    else
  	  detail = EXPRESS_GATEWAY.details_for(checkout.express_token)
      response = EXPRESS_GATEWAY.refund(amount, detail.authorization)
    end

  	if response.success?
  		checkout.status = "Refunded"
  		checkout.user_refund.confirm_at = Time.now
  		checkout.user_refund.amount = params[:amount].to_i
  		checkout.user_refund.save
  		checkout.save
  		redirect_to admin_all_refunds_url, notice: {:type => 'success', :msg => 'Refund successfully.'}
  	else
  		redirect_to admin_all_refunds_url, notice: {:type => 'error', :msg => response.message}
  	end
  end
end