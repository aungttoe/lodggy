class Admin::CheckoutsController < AdminController
  before_action :set_checkout, except: [:index, :user]

  def index
    if current_user.role.eql?("hotel")
      @checkouts = Checkout.joins(:property).where("properties.user_id = ?", current_user.id)
    else
      @checkouts = Checkout.all
    end

    if params[:search]
      if !params[:search][:status].empty?
        if !params[:search][:status].eql?("All Status")
          @checkouts = @checkouts.where(:status => params[:search][:status])
        end
      end

      if !params[:search][:email].empty?
        @checkouts = @checkouts.where("email_id ILIKE ?", "%#{params[:search][:email]}%")
      end

      if !params[:search][:booking_id].empty?
        @checkouts = @checkouts.where(:booking_id => params[:search][:booking_id])
      end

      if !params[:search][:created_at].empty?
        date = params[:search][:created_at].to_date
        @checkouts = @checkouts.where("created_at >= ? AND created_at <= ?", date, date+1.day)
      end
    end

    @checkouts = @checkouts.order(:created_at => :desc)
  	@checkouts = @checkouts.page(params[:page])
  end

  def user
    @checkouts = Checkout.where(:user_id => params[:user_id]).page(params[:page])
    render "index"
  end

  def detail
    activities = PublicActivity::Activity.where(:trackable_type => "Checkout",:trackable_id => @checkout.id)
    if current_user.role.eql?("hotel")
      activities.each do |activity|
        if !activity.is_read_hotel
          activity.is_read_hotel = true
          activity.save
        end
      end
    else
      activities.each do |activity|
        if !activity.is_read_admin
          activity.is_read_admin = true
          activity.save
        end
      end
    end
  end

  def confirm
  	checkout = Checkout.find(params[:id])
  	total = checkout.total_cart * 100
  	if checkout.status.eql?("Not Confirm")
      if checkout.express_token.nil?
        purchase = STANDARD_GATEWAY.capture(total, checkout.authorization_id )
        checkout.transaction_id = purchase.params["transaction_id"]
      else
    		purchase = EXPRESS_GATEWAY.purchase(total, 
          :ip => request.ip,
          :token => checkout.express_token,
          :payer_id => checkout.express_payer_id
        )
      end

  		if purchase.success?
        checkout.status = "Confirmed"
        checkout.save

        pdf = WickedPdf.new.pdf_from_string(render_to_string('contact_us/voucher_hotel', :layout => false, :locals => {:checkout => checkout}))
  			ContactUs.confirm_order_user(checkout, pdf).deliver
  			redirect_to admin_all_checkouts_url, notice: {:type => 'success', :msg => 'Checkout was successfully updated.'}
  		else
  			redirect_to admin_all_checkouts_url, notice: {:type => 'error', :msg => 'Checkout unsuccessfully to update.'}
  		end
  	else
  		redirect_to admin_root_path
  	end
  end

  private

  def set_checkout
    id = params[:id] || params[:checkout_id]
    @checkout = Checkout.find(id)
    unless @checkout.property.user_id.eql?(current_user.id)
      render("public/404_admin") if current_user.role.eql?("hotel")
    end
  end
end
