class Admin::RoomsController < AdminController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :set_property, :set_city
  before_action :manage_facilities, only: [:edit, :new]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = @property.rooms
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
  end

  # GET /rooms/new
  def new
    @room = @property.rooms.new
    @pay = ['Dorm', 'Private']
    @cancellation = ["Flexible", "Moderate", "Strict"]
    @room_types = RoomType.all.map {|type| [type.name, type.id]}
    # @room_types = ['Single Room', 'Twin Room', 'Double Room', 'Triple Room', 'Family Room', 'Mixed Dorm', 'Male Dorm', 'Female Dorm']
  end

  # GET /rooms/1/edit
  def edit
    @pay = ['Dorm', 'Private']
    @cancellation = ["Flexible", "Moderate", "Strict"]
    @room_types = RoomType.all.map {|type| [type.name, type.id]}
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = @property.rooms.new(room_params)

    respond_to do |format|
      if @room.save
        @room.property.add_price
        format.html { redirect_to [:admin, @city,@property,@room], notice: {:type => 'success', :msg => 'Room was successfully created.'} }
        format.json { render action: 'show', status: :created, location: @room }
      else
        format.html { render action: 'new', notice: {:type => 'error', :msg => 'Room unsuccessfully created.'} }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        @room.property.add_price
        format.html { redirect_to [:admin, @city,@property,@room], notice: {:type => 'success', :msg => 'Room was successfully updated.'} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit', notice: {:type => 'error', :msg => 'Room unsuccessfully updated.'} }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    @property.add_price
    respond_to do |format|
      format.html { redirect_to admin_city_property_rooms_url(@city, @property), notice: {:type => 'success', :msg => "Room was successfully deleted."} }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def manage_facilities
      facilities = ['Shampoo', 'Bathroom Essentials', 'Hair Dryer', 'Water Heater', 'Air Conditioning', 'TV', 'Adaptors', 'Reading Lights', 'Keycard Access', 'Phone', 'Smoke Detector']
      @facilities = Facility.where(:facility_name => facilities)
    end

    def set_room
      @room = Room.find(params[:id])
    end

    def set_property
      @property = Property.friendly.find(params[:property_id])
      if current_user.role.eql?("hotel")
        render("public/404_admin") unless @property.user_id.eql?(current_user.id)
      end

    end

    def set_city
      @city = City.friendly.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:title, :ensuite, :about, :price, :count, :guest, :instant, :photo, :cancellation, {:facility_ids => []})
    end

    def my_sanitizer
      params.require(:room).permit(:name)
    end
end
