class Admin::ActivitiesController < AdminController
  def index
  	if current_user.role.eql?("admin")
  		@all_notifications = PublicActivity::Activity.order(:created_at => :desc)
      @all_notifications.map {|notification| notification.update(:is_read_admin => true)}
  	else
  		@all_notifications = PublicActivity::Activity.where(:owner_id => current_user.id)
      @all_notifications.map {|notification| notification.update(:is_read_hotel => true)}
  	end
  	@all_notifications = @all_notifications.page(params[:page]).per(10)
  end
end
