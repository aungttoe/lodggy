class Admin::DashboardController < AdminController
	before_action :authenticate_user!
	
	def index
		if current_user.role.eql?("hotel")
			@checkouts = Checkout.joins(:property).where("properties.user_id = ?", current_user.id).where(:status => 'Confirmed')
			@properties = Property.where(:user_id => current_user.id)
		else
			@checkouts = Checkout.where(:status => 'Confirmed')
			@properties = Property.all
		end

		@total_price = @checkouts.map {|checkout| checkout.total_cart }
		@total_price = @total_price.inject(:+)
		from = params[:from] ? params[:from].to_date : nil
		to = params[:to] ? params[:to].to_date : nil
		if from && to
			data = Visit.group_by_day(:started_at, format: "%B, %d", range: from..to).count
		else
			data = Visit.group_by_day(:started_at, format: "%B, %d").count
		end

		@chart = LazyHighCharts::HighChart.new('graph') do |f|
			f.title(:text => "Total Visitor #{data.values.inject(:+)}")
	    f.options[:xAxis][:categories] = data.keys
	    f.series(:type=> 'spline', :name => "Visitor", :data=> data.values)
    end
	end

	def search
		@query = params[:query]
		@users = User.search(@query).page(params[:page]).per(10)
		@checkouts = Checkout.search(@query).page(params[:page]).per(10)
		@properties = Property.search(@query).page(params[:page]).per(10)
	end
end
