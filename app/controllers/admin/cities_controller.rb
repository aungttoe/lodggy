class Admin::CitiesController < AdminController
  before_action :set_city, :except => [:index, :new, :create]
  before_action :check_role
  load_and_authorize_resource param_method: :my_sanitizer

  def index
    @cities = City.all
    
    @hash = []
    @cities.map {|p| @hash << {:latlng => [p.lat, p.lng]} }
    @panTo = []
    @cities.map {|p| @panTo << [p.lat, p.lng]}
  end

  def show
  end

  def new
    @city = City.new
  end

  def edit
  end

  def create
   @city = City.new(city_params)

    respond_to do |format|
      if @city.save
        format.html { redirect_to admin_cities_url, notice: {:type => 'success', :msg => 'City was successfully created.'} }
        format.json { render action: 'show', status: :created, location: @city }
      else
        format.html { render action: 'new'}
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to admin_cities_url, notice: {:type => 'success', :msg => 'City was successfully updated.'} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit'}
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to admin_cities_url, notice: {:type => 'success', :msg => 'City was successfully deleted.'} }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:address, :name, :hotel_id, :description, :user_email, :place_id, :cityimg, :total, :density, :ethnicities, :religions, :country, :lat, :lng, :state, :town)
    end

    def my_sanitizer
      params.require(:city).permit(:name)
    end
end
