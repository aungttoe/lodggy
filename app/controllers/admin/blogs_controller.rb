class Admin::BlogsController < AdminController
	before_action :set_blog, only: [:show, :edit, :update, :destroy, :destroy_comment]
  after_action :destroy_tags, only: [:create, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /blogs
  # GET /blogs.json
  def index
    @blogs = Blog.order(:created_at => :desc).page(params[:page])
  end

  # GET /blogs/1
  # GET /blogs/1.json
  def show
    @comments = @blog.blog_comments
  end
  # GET /blogs/new
  def new
    @blog = Blog.new
  end

  # GET /blogs/1/edit
  def edit
  end

  # POST /blogs
  # POST /blogs.json
  def create
    @blog = Blog.new(blog_params)

    respond_to do |format|
      if @blog.save
        format.html { redirect_to admin_blogs_url, notice: 'Blog was successfully created.' }
        format.json { render action: 'show', status: :created, location: [@city, @blog] }
      else
        format.html { render action: 'new' }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blogs/1
  # PATCH/PUT /blogs/1.json
  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to admin_blogs_url, notice: 'Blog was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blogs/1
  # DELETE /blogs/1.json
  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to admin_blogs_url }
      format.json { head :no_content }
    end
  end

  def destroy_comment
    @comment = BlogComment.find(params[:comment_id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to admin_blog_url(@blog) }
    end
  end

  private
    def set_blog
      @blog = Blog.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_params
      params.require(:blog).permit(:title, :body, :image, :tag_list)
    end

    def my_sanitizer
      params.require(:blog).permit(:name)
    end

    def destroy_tags
      tags = ActsAsTaggableOn::Tag.where(:taggings_count => 0)

      if !tags.blank?
        tags.destroy_all
      end
    end
end
