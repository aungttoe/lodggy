class Admin::UsersController < AdminController
  before_action :set_user, :only => [:show, :edit, :destroy, :update]
  before_action :check_role, only: [:index]
  before_action :check_role_except_owner, :except => :index

  def index
    @users = User.order(:created_at => :desc)

    if params[:email]
      @users = @users.where("email LIKE ?", "%#{params[:email]}%")
    end
    if params[:role]
      @users = @users.where(:role => params[:role]) if !params[:role].empty?
    end

    @users = @users.page(params[:page])
  end

  def show
    activity = PublicActivity::Activity.where(:trackable_id => @user.id, :trackable_type => "User").first
    if !activity.nil?
      activity.update(:is_read_admin => true) if !activity.is_read_admin?
    end
  end
  def edit
    
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.confirmed_at = Time.now

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_user_url(@user), notice: 'City was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    respond_to do |format|
      if @user.update_attributes(user_params)
        if current_user == @user
          sign_in(@user, :bypass => true)
        end
        format.html { redirect_to admin_user_url(@user), notice: 'City was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html {redirect_to admin_users_url}
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :gender, :address, :mobile, :role, :avatar)
  end
  def check_role_except_owner
    if current_user.role.eql?("hotel")
      unless @user.eql?(current_user)
        render("public/404_admin")
      end
    end
  end
end
