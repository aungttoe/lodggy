class Admin::PageController < AdminController
  before_action :check_role

  def cancellation
  	@page = Page.where(:name => 'cancellation').first
  end

  def terms
  	@page = Page.where(:name => 'terms').first
  end

  def story
    @page = Page.where(:name => 'story').first
  end

  def privacy
    @page = Page.where(:name => 'privacy').first
  end

  def homepage
    @page = Page.where(:name => 'homepage').first
  end

  def update
  	@page = Page.where(:name => params[:name]).first
  	@page.content = params[:content]
  	if @page.save
  		redirect_to :back, notice: {:type => 'success', :msg => 'Checkout was successfully updated.'}
  	else
  		redirect_to :back, notice: {:type => 'error', :msg => 'Checkout was failed to update.'}
  	end

  end
end
