class Admin::PlacesController < AdminController
  before_action :check_role
	before_action :set_place, only: [:show, :edit, :update, :destroy]
  before_action :set_city, only: [:show, :edit,:create, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /places
  # GET /places.json
  def index
    @cities = City.all
    if params[:city_id]
      set_city
      @places = @city.places
    else
      @places = Place.all
    end

    @hash = []
    @places.map {|p| @hash << {:latlng => [p.lat, p.lng]} }
    @panTo = []
    @places.map {|p| @panTo << [p.lat, p.lng]}

  end

  # GET /places/1
  # GET /places/1.json
  def show
    @cities = City.all
    @places = Place.where(:city_id => params[:city_id]).all

    @hash = Gmaps4rails.build_markers(@place) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # GET /places/new
  def new
    @city = City.friendly.find(params[:city_id])
    @place = @city.places.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create

    @city = City.friendly.find(params[:city_id])
    @place = @city.places.new(place_params)

    respond_to do |format|
      if @place.save
        format.html { redirect_to admin_city_places_url(@city), notice: {:type => 'success', :msg => "Place was successfully created."} }
        format.json { render action: 'show', status: :created, location: [@city,@place] }
      else
        format.html { render action: 'new' }
        format.json { render json: [@city,@place].errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params.merge(city_id: @city.id))
        format.html { redirect_to admin_city_places_url(@city), notice: {:type => 'success', :msg => "Place was successfully updated."} }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: [@city,@place].errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to admin_city_places_url(@city), notice: {:type => 'success', :msg => "Place was successfully deleted."} }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.friendly.find(params[:id])
    end

    def set_city
      @city = City.friendly.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      params.require(:place).permit(:title, :content, :city_id, :address, :town, :state, :country, :placeimg, :lat, :lng)
    end

    def my_sanitizer
      params.require(:place).permit(:name)
    end
end
