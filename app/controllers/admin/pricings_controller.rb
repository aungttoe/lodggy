class Admin::PricingsController < AdminController
	before_action :set_room_date, except: [:index, :create, :no_room]

	def index
		@city = City.friendly.find(params[:city_id])
		@property = Property.friendly.find(params[:property_id])
		@room = Room.find(params[:room_id])

		@room = Room.find(params[:room_id])
		@room_dates = RoomDate.where(:room_id => params[:room_id])
		@dates = []
		@room_dates.each do |date|
			if date.type_date.eql?("unavailable")
				title = "Not Available (#{date.no_room})"
				color = "red"
				editable = true
			elsif date.type_date.eql?("booked")
				title = "Booked (#{date.no_room})"
				color = "#4DB3A4"
				editable = false
				url = {:url => admin_checkout_detail_url(date.checkout_id)}
			else
				title = "Price per Night $#{date.price}.00"
				color = "#25adf3"
				editable = true
			end

			hash = {:title => title, :start => date.from, :end => date.to, :color => color, :editable => editable, :id => date.id}
			
			if date.type_date.eql?("booked")
				hash.merge!(url)
			end
			@dates << hash
		end
	end

	def edit
		room = Room.find(params[:room_id])
		dates = RoomDate.joins(:room).where(:room_id => params[:room_id]).where("type_date IN (?)", ['unavailable', 'booked']).where("? >= room_dates.from AND ? <= room_dates.to", @dates.from, @dates.to)
		if dates.empty?
			@max = room.count
		else
			booked = dates.pluck(:no_room).inject(:+)
			@max = room.count - booked
			@max += @dates.no_room
		end

		@from = @dates.from.to_formatted_s(:long)
		@to = (@dates.to - 1.day).to_formatted_s(:long)


		respond_to do |format|
			format.json
		end
	end

	def create
		room = Room.find(params[:room_id])

		from_date = params[:from].to_date
		to_date = params[:to].to_date + 1.day
		@dates = RoomDate.new
		@dates.from 			= from_date
		@dates.to 				= to_date
		@dates.room_id 		= room.id
		@dates.no_room 		= params[:no_room].to_i
		@dates.type_date 	= params[:date_type]
		@dates.price 			= params[:price]
		@dates.no_room 		= params[:no_room]
		@dates.note				= params[:note]
		@dates.save

		respond_to do |format|
			format.js
		end
	end

	def update
		if params[:update_type].eql?('modal')
			@dates.from = params[:from].to_date
			@dates.to = params[:to].to_date + 1.day
			@dates.note = params[:note]
			@dates.price = params[:price]
			@dates.no_room = params[:no_room]
		else
			from = params[:start].to_date
			to   = params[:end].to_date

			@dates.from 	= from
			@dates.to 		= to
		end

		@dates.save

		respond_to do |format|
			format.json
			format.js
		end
	end

	def destroy
		@dates.destroy
		respond_to do |format|
			format.js
		end
	end

	def no_room
		room = Room.find(params[:room_id])
		from = params[:from].to_date
		to = params[:to].to_date
		dates = RoomDate.joins(:room).where(:room_id => room.id).where("type_date IN (?)", ['unavailable', 'booked']).where("? >= room_dates.from AND ? <= room_dates.to", from, to)
		if dates.empty?
			@max = room.count
		else
			booked = dates.pluck(:no_room).inject(:+)
			@max = room.count - booked
		end
		@from = from.to_formatted_s(:long)
		@to = (to - 1.day).to_formatted_s(:long)
		respond_to do |format|
			format.json
		end
	end

	private
		def set_room_date
			@dates = RoomDate.find(params[:id])
		end
end

