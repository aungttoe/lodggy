class Admin::PropertiesController < AdminController
	before_action :set_city, except: [:index, :new_facility]
	before_action :set_property, except: [:index, :published, :new, :create, :deals, :new_facility]
	before_action :set_variables, only: [:new, :edit]

  def index
    @cities = City.all

    if current_user.role.eql?("hotel")
      @properties = Property.with_user(current_user.id)
    else
      @properties = Property.all
    end

    if params[:city_id]
      city = @cities.friendly.find(params[:city_id])
      @properties = @properties.where(:city_id => city.id)
    end

    @properties = @properties.includes(:rooms).order(:created_at => :desc).page(params[:page])

    @hash = []
    @properties.map {|p| @hash << {:latlng => [p.lat, p.lng]} }
    @panTo = []
    @properties.map {|p| @panTo << [p.lat, p.lng]}
  end

  def new
  	@property = Property.new
  end

  def edit
  end

  def show 
  end

  def create
  	@property = Property.new(property_params.merge(:city_id => @city.id, :user_id => current_user.id))

  	respond_to do |format|
     	if @property.save
        if params[:images]
          params[:images].each { |image|
          @property.photos.create(image: image)
          }
        end
        format.html { redirect_to admin_all_properties_path }
        
      else
        format.html { render action: 'new' }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  	respond_to do |format|
      if @property.update(property_params)

        if params[:images]
            params[:images].each do |image|
              @property.photos.create(image: image, property_id: @property.id)
            end
        end
        if params[:remove]
          Photo.where("id IN(?)", params[:remove]).destroy_all
        end
        format.html { redirect_to admin_city_properties_path(@property.city) }
      else
        format.html { render action: 'edit' }
        format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  def new_facility
    @facility = Facility.new(facility_params)
    @facility.save
    respond_to do |format|
      format.js
    end
  end

  def destroy
  	@property.destroy
    respond_to do |format|
      format.html { redirect_to admin_all_properties_url }
    end
  end

  def deals
  	property = Property.friendly.find(params[:property_id])
  	if property.deals?
  		property.deals = false
  	else
  		property.deals = true
  	end
  	property.save
  	redirect_to admin_city_properties_path(property.city)
  end

  def published
    property = Property.friendly.find(params[:property_id])
    if property.published?
      property.published = false
    else
      property.published = true
    end
    property.save
    redirect_to admin_city_properties_path(property.city)
  end

  private

  def set_city
  	@city = City.friendly.find(params[:city_id])
  end

  def set_property
  	@property = Property.friendly.find(params[:id])
    if current_user.role.eql?("hotel")
      render("public/404_admin") unless @property.user_id.eql?(current_user.id)
    end
  end

  def set_variables
  	@room_types = [["Entire Place"],["Private Room"],["Shared Room"]]
    @property_types = [["Hostel"], ["Bed and Breakfast"], ["Hotel"], ["Inn"], ["Motel"]]
  	@currencies = [["EUR",'Euro'], ['US Dollar', 'USD']]
  	@language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @cancellation = ["Flexible", "Moderate", "Strict"]

    facilities = ['Shampoo', 'Bathroom Essentials', 'Hair Dryer', 'Water Heater', 'Air Conditioning', 'TV', 'Adaptors', 'Reading Lights', 'Keycard Access', 'Phone', 'Smoke Detector']
    @facilities = Facility.where.not(:facility_name => facilities).where(:facility_type => "Facility")
  end
  
  def property_params
  	params.require(:property).permit(:check_in, :check_out, :address, :guests,  :address_two, :bedrooms, :city_id, :account_number, :account_name, :bank_name, :swift, :bank_address, :contact_name, :content, :currency, :description, :directions, :email, :language, :lat, :lng, :location, :phone, :postal_code, :price, :property_type, :room_type, :state, :things, :title, :website, :cancellation, {:facility_ids => []})
  end

  def facility_params
    params.require(:facility).permit(:facility_name, :facility_type)
  end
end
