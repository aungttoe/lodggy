class Admin::FaqsController < AdminController
  before_action :set_faq, only: [:show, :edit, :update, :destroy, :new_child, :child]

  def index
    @faqs = Faq.where(:parent_id => nil)
    @faq = Faq.new
  end

  def show
    @child_faqs = Faq.where(:parent_id => @faq)
  end

  def new
    @faq = Faq.new
  end

  def edit
    respond_to do |format|
      format.js
    end
  end

  def child
    
  end

  def new_child
    @child = Faq.new
  end

  def create_child
    @faq = Faq.new(faq_params)
    @faq.parent_id = params[:id]
    respond_to do |format|
      if @faq.save
        if params[:id]
          format.html { redirect_to admin_faq_url(params[:id]), notice: 'Faq was successfully created.' }
        else
          format.html { redirect_to admin_faqs_url, notice: 'Faq was successfully created.' }
        end
        format.json { render action: 'show', status: :created, location: @faq }
      else
        format.html { render action: 'new' }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  def create
    @faq = Faq.new(faq_params)
    @faq.parent_id = params[:id] if params[:id]
    respond_to do |format|
      if @faq.save
        if params[:id]
          format.html { redirect_to admin_faq_url(@faq.parent_id), notice: 'Faq was successfully created.' }
        else
          format.html { redirect_to admin_faqs_url(@faq), notice: 'Faq was successfully created.' }
        end
        format.json { render action: 'show', status: :created, location: @faq }
      else
        format.html { render action: 'new' }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @faq.update(faq_params)
        if @faq.parent_id.nil?
          format.html { redirect_to admin_faqs_url, notice: 'Faq was successfully updated.' }
        else
          format.html { redirect_to admin_faq_url(@faq.parent_id), notice: 'Faq was successfully updated.' }
        end
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @faq.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    faqs = Faq.where(:parent_id => @faq.id)
    if !faqs.empty?
      faqs.destroy_all
    end
    @faq.destroy
    respond_to do |format|
      if faqs.empty?
        format.html { redirect_to admin_faqs_url }
      else
        format.html { redirect_to child_admin_faq_url(@faq.parent_id) }
      end
      format.json { head :no_content }
    end
  end

  private
    def set_faq
      @faq = Faq.find(params[:id])
    end

    def faq_params
      params.require(:faq).permit(:name, :content)
    end
end
