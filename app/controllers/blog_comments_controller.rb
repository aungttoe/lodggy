class BlogCommentsController < ApplicationController

  def create
    blog = Blog.friendly.find(params[:id])
    name = params[:name]
    body = params[:body]
    @comment = BlogComment.create(:name => name, :body => body, :blog_id => blog.id)
    
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @comment = BlogComment.find(params[:id])
    @city = @comment.blog.city
    @blog = @comment.blog
    if @comment.present?
      @comment.destroy
    end
    redirect_to [@city, @blog]
  end

  private

    def comment_params
      params.require(:blog_comment).permit(:name, :body, :blog_id)
    end
end
