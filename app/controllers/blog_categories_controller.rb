class BlogCategoriesController < ApplicationController
  before_action :set_blog_category, only: [:show, :edit, :update, :destroy, :map_blog, :blogs_cat]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /blog_categories
  # GET /blog_categories.json
  def index
    @city = City.friendly.find(params[:city_id])
    @blog_categories = BlogCategory.all
  end

  # GET /blog_categories/1
  # GET /blog_categories/1.json
  def show
    @blogs = Blog.joins(:blog_category).where("blog_categories.id = ? AND cities.id = ?", params[:id], @city.id).page(params[:page]).per(3)
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:id => params[:id])
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @cities = City.all
    @places = Place.joins(:city).where("cities.id = ?", @city.id)

    @hash = Gmaps4rails.build_markers(@places) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def blogs_cat
    @blogs = Blog.joins(:blog_category, :city).where("blog_categories.id = ? AND cities.id = ?", params[:id], @city.id).page(params[:page]).per(3)
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:id => params[:id]).all
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @cities = City.all
    @places = Place.joins(:city).where("cities.id = ? ", @city.id)

    @hash = Gmaps4rails.build_markers(@places) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def map_blog
    @blogs = Blog.where(:blog_category_id => params[:id]).where(:city_id => @city.id)
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:id => params[:id])
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @cities = City.all
    @places = Place.joins(:city).where("cities.id = ?", @city.id).page(params[:page]).per(15)

    @hash = Gmaps4rails.build_markers(@places) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # GET /blog_categories/new
  def new
    @city = City.friendly.find(params[:city_id])
    @blog_category = BlogCategory.new
  end

  # GET /blog_categories/1/edit
  def edit
  end

  # POST /blog_categories
  # POST /blog_categories.json
  def create
    @blog_category = BlogCategory.new(blog_category_params)
    @city = City.friendly.find(params[:city_id])
    respond_to do |format|
      if @blog_category.save
        format.html { redirect_to [@city, @blog_category], notice: 'Blog category was successfully created.' }
        format.json { render action: 'show', status: :created, location: [@city, @blog_category] }
      else
        format.html { render action: 'new' }
        format.json { render json: @blog_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /blog_categories/1
  # PATCH/PUT /blog_categories/1.json
  def update
    respond_to do |format|
      if @blog_category.update(blog_category_params)
        format.html { redirect_to [@city, @blog_category], notice: 'Blog category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @blog_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /blog_categories/1
  # DELETE /blog_categories/1.json
  def destroy
    @blog_category.destroy
    respond_to do |format|
      format.html { redirect_to action: 'index' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_blog_category
      @blog_category = BlogCategory.find(params[:id])
      @city = City.friendly.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def blog_category_params
      params.require(:blog_category).permit(:name, :blog_id)
    end

    def my_sanitizer
      params.require(:blog_category).permit(:name)
    end
end
