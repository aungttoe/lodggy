class Dashboard::UsersController < DashboardController
  include Dashboard::UsersHelper
  before_action :set_property, only: [:checkouts, :checkout_detail]
  before_action :cancellation, only: [:confirm_cancel, :cancel]

  def index
  	@genders = [['Male', 'Male'], ['Female', 'Female']]
    @comments = Comment.where(user_id: current_user.id)
    @properties = Property.where(user_id: current_user.id).includes(:checkouts)
    @bookings = Checkout.where(user_id: current_user.id).order(:created_at => :desc).page(params[:page]).per(6)
    
  end

  def checkouts
  	@checkouts = Checkout.where(:property_id => @property.id).includes(:user)
  end

  def checkout_detail
  	@checkout = Checkout.find(params[:id])
  end

  def details
    @checkout = Checkout.find(params[:id])
    if params[:token] && session[:params]
      purchase = EXPRESS_GATEWAY.purchase((session[:params][:amount] * 100), 
        :ip => request.ip,
        :token => params[:token],
        :payer_id => @checkout.express_payer_id
      )
      if purchase.success?
        @checkout.status = "Changed"
        if session[:params][:type].eql?("no_room")
          night = @checkout.nights
          no_room = session[:params][:no_room].to_i
          @checkout.no_room = session[:params][:no_room]
        elsif session[:params][:type].eql?("move_date") || session[:params][:type].eql?("change_date")
          night = (session[:params][:check_out].to_date - session[:params][:check_in].to_date).to_i
          no_room = @checkout.no_room
          @checkout.nights = night
          @checkout.check_in = session[:params][:check_in]
          @checkout.check_out = session[:params][:check_out]
        end
        @checkout.total_price = change_total_price(night, no_room, @checkout.per_night)
        @checkout.government = change_government(night, no_room, @checkout.per_night)
        @checkout.service = change_service(night, no_room, @checkout.per_night)
        @checkout.total_cart = change_total_cart(night, no_room, @checkout.per_night)

        @checkout.save

        @checkout.create_activity(:change_date, :owner => @checkout.property.user)
        ContactUs.change(@checkout, session[:params]).deliver
        ContactUs.change_admin(@checkout, session[:params]).deliver
        flash[:notice] = "Your checkout successfully changed."
        session[:params] = nil
      else
        flash[:notice] = purchase.message
      end
    end
    
    unless @checkout.user_id.eql?(current_user.id)
      render "public/404"
    end
  end

  def confirm_cancel
    
  end
  
  def cancel
    amount = @refund * 100
    if @checkout.express_token.nil?
      response = STANDARD_GATEWAY.refund(amount, @checkout.transaction_id)
    else
      detail = EXPRESS_GATEWAY.details_for(@checkout.express_token)
      response = EXPRESS_GATEWAY.refund(amount, detail.authorization)
    end
    if response.success?
      @checkout.status = "Refunded"

      refund = @checkout.build_user_refund
      refund.confirm_at = Time.now
      refund.amount = @refund
      refund.save
      @checkout.save
      flash[:notice] = "Checkout refunded."

      ContactUs.cancel(@checkout).deliver
      ContactUs.cancel_admin(@checkout).deliver
      @checkout.create_activity(:cancel, :owner => @checkout.property.user)
      redirect_to dashboard_details_path(@checkout)
    else
      flash[:notice] = "There is a problem with your checkout."
      redirect_to dashboard_confirm_cancel_url
    end
  end

  def check_available
    from = params[:from].to_date
    to = params[:to].to_date
    @checkout = Checkout.find(params[:id])
    room = @checkout.room
    dates = RoomDate.where("? >= room_dates.from AND ? <= room_dates.to", from, to).where("type_date IN (?)", ['unavailable', 'booked'])
    arr = []
    booked = dates.pluck(:no_room).inject(:+)
    if !booked.nil?
      if booked < room.count
        available = true
      else
        available = false
      end
    else
      available = true
    end

    respond_to do |format|
      format.json {render :json => {"available" => available}}
    end
  end

  def change
    @checkout = Checkout.includes(:room).find(params[:id])
    if @checkout.status.eql?("Changed") || @checkout.status.eql?("Refunded")
      render("public/404")
    end
    @dates = RoomDate.where("? >= room_dates.from AND ? <= room_dates.to", @checkout.check_in, (@checkout.check_out + 1.day)).where("type_date IN (?)", ['unavailable', 'booked'])
    no_rooms = (@dates - [@checkout.room_date])
    rooms = 0
    no_rooms = no_rooms.map{|room| rooms += room.no_room}
    no_rooms = (no_rooms.inject(:+)).to_i
    @no_rooms = @checkout.room.count - no_rooms
    @range = @checkout.check_out - @checkout.check_in
    
  end

  def change_date
    @checkout = Checkout.includes(:room).find(params[:id])
    room = @checkout.room
    
    if params[:type].eql?("change_date")
      night = (params[:check_out].to_date - params[:check_in].to_date).to_i
      if @checkout.free_change?
        if night > @checkout.nights
          #increase nights
          @total = change_total_cart(night, @checkout.no_room, @checkout.per_night)
          amount = change_total_cart(night, @checkout.no_room, @checkout.per_night) - @checkout.total_cart
          session[:params] = {:amount => amount, :check_in => params[:check_in].to_date, :check_out => params[:check_out].to_date, :type => params[:type], :way => 'free_increase'}
        elsif night == @checkout.nights
          #some nights
          @checkout.check_in = params[:check_in].to_date
          @checkout.check_out = params[:check_out].to_date
          @checkout.status = "Changed"
          @checkout.save

          session[:params] = {:type => params[:type], :way => "free_change"}

          @checkout.create_activity(:change_date, :owner => @checkout.property.user)
          ContactUs.change(@checkout, params).deliver
          ContactUs.change_admin(@checkout, params).deliver

          flash[:notice] = "Your booking date successfully changed"
        else
          #decrease nights
          amount = @checkout.total_price - change_total_cart(night, @checkout.no_room, @checkout.per_night)

          if @checkout.express_token.nil?
            response = STANDARD_GATEWAY.refund(amount * 100, @checkout.transaction_id)
          else
            detail = EXPRESS_GATEWAY.details_for(@checkout.express_token)
            response = EXPRESS_GATEWAY.refund(amount * 100, detail.authorization)
          end

          if response.success?
            @checkout.nights = night
            @checkout.check_in = params[:check_in].to_date
            @checkout.check_out = params[:check_out].to_date
            #change prices
            @checkout.total_price = change_total_price(night, @checkout.no_room, @checkout.per_night)
            @checkout.service = change_service(night, @checkout.no_room, @checkout.per_night)
            @checkout.government = change_government(night, @checkout.no_room, @checkout.per_night)
            @checkout.total_cart = change_total_cart(night, @checkout.no_room, @checkout.per_night)
            
            @checkout.status = "Changed"
            #create record refund
            refund = @checkout.build_user_refund
            refund.confirm_at = Time.now
            refund.amount = amount
            refund.save
            @checkout.save

            @checkout.create_activity(:change_date, :owner => @checkout.property.user)
            ContactUs.change(@checkout, params).deliver
            ContactUs.change_admin(@checkout, params).deliver

            flash[:notice] = "Your booking date has been changed."
          else
            if detail.authorization.nil?
              flash[:notice] = detail.message
            else
              flash[:notice] = response.message
            end
          end

          session[:params] = {:type => params[:type], :way => 'free_decrease'}
        end
      else
        price = @checkout.per_night
        no_room = @checkout.no_room
        if night > @checkout.nights
          @total = change_total_cart(night, no_room, price)
          amount = (@total + price) - @checkout.total_cart
          session[:params] = {:amount => amount, :check_in => params[:check_in].to_date, :check_out => params[:check_out].to_date, :type => params[:type], :way => 'charge_increase'}
        elsif night == @checkout.nights
          session[:params] = {:amount => price, :check_in => params[:check_in].to_date, :check_out => params[:check_out].to_date, :type => params[:type], :way => 'charge'}
        else
          total_cart = change_total_cart(night, no_room, price)
          total_cart_with_price = total_cart + price
          amount = @checkout.total_cart - total_cart_with_price

          if @checkout.express_token.nil?
            response = STANDARD_GATEWAY.refund(amount * 100, @checkout.transaction_id)
          else
            detail = EXPRESS_GATEWAY.details_for(@checkout.express_token)
            response = EXPRESS_GATEWAY.refund(amount * 100, detail.authorization)
          end

          if response.success?
            @checkout.nights = night
            @checkout.check_in = params[:check_in].to_date
            @checkout.check_out = params[:check_out].to_date
            #save new prices
            @checkout.total_price = change_total_price(night, no_room, price)
            @checkout.service = change_service(night, no_room, price)
            @checkout.government = change_government(night, no_room, price)
            @checkout.total_cart = change_total_cart(night, no_room, price)

            @checkout.status = "Changed"

            refund = @checkout.build_user_refund
            refund.confirm_at = Time.now
            refund.amount = amount
            refund.save
            @checkout.save

            @checkout.create_activity(:change_date, :owner => @checkout.property.user)
            ContactUs.change(@checkout, params).deliver
            ContactUs.change_admin(@checkout, params).deliver

            flash[:notice] = "Your booking date has been changed."
          else
            if detail.authorization.nil?
              flash[:notice] = detail.message
            else
              flash[:notice] = response.message
            end
          end
          session[:params] = {:type => params[:type], :way => 'charge_decrease'}
        end
      end
    end

    if params[:type].eql?("move_date")
      if @checkout.free_change?
        @checkout.check_in = params[:check_in].to_date
        @checkout.check_out = params[:check_out].to_date
        @checkout.status = "Changed"
        @checkout.save

        @checkout.create_activity(:change_date, :owner => @checkout.property.user)
        ContactUs.change(@checkout, params).deliver
        ContactUs.change_admin(@checkout, params).deliver

        session[:params] = {:type => params[:type]}
        flash[:notice] = "Your booking date successfully changed."
      else
        price = @checkout.per_night
        session[:params] = {:amount => price, :check_in => params[:check_in].to_date, :check_out => params[:check_out].to_date, :type => params[:type]}
      end
    end

    if params[:type].eql?("no_room")
      no_room = params[:no_room].to_i
      price = @checkout.per_night
      if no_room > @checkout.no_room
        @total = change_total_cart(@checkout.nights, no_room, price)

        session[:params] = {:amount => (@total - @checkout.total_cart), :no_room => params[:no_room], :type => params[:type], :way => 'increase'}
      else
        total = @checkout.total_price
        new_total = change_total_cart(@checkout.nights, no_room, price)

        amount = total - new_total
        unless @checkout.free_change?
          amount -= price
        end

        if @checkout.express_token.nil?
          response = STANDARD_GATEWAY.refund(amount, @checkout.transaction_id)
        else
          detail = EXPRESS_GATEWAY.details_for(@checkout.express_token)
          response = EXPRESS_GATEWAY.refund(amount, detail.authorization)
        end

        if response.success?
          @checkout.total_price = change_total_price(@checkout.nights, no_room, price)
          @checkout.service = change_service(@checkout.nights, no_room, price)
          @checkout.government = change_government(@checkout.nights, no_room, price)
          @checkout.total_cart = new_total

          @checkout.no_room = params[:no_room].to_i
          @checkout.status = "Changed"

          refund = @checkout.build_user_refund
          refund.confirm_at = Time.now
          refund.amount = amount
          refund.save
          @checkout.save

          @checkout.create_activity(:change_date, :owner => @checkout.property.user)
          ContactUs.change(@checkout, params).deliver
          ContactUs.change_admin(@checkout, params).deliver

          flash[:notice] = "Your no of rooms checkout has changed."
        end

        session[:params] = {:type => params[:type], :way => 'decrease'}
      end
    end
  end

  def payment
    checkout = Checkout.find(params[:id])

    amount = session[:params][:amount] * 100
    if params[:pay_type].eql?("paypal")
      response = EXPRESS_GATEWAY.setup_purchase(amount,
        :ip                => request.remote_ip,
        :return_url        => dashboard_details_url(checkout),
        :cancel_return_url => dashboard_change_date_url(checkout),
        :currency => "USD",
        :items => [{
            :description => "Change Checkout",
            :amount => amount
          }]
      )
      redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
    end

    if params[:pay_type].eql?("card")
      credit_card = ActiveMerchant::Billing::CreditCard.new(
        :number             => params[:number],
        :verification_value => params[:verification_value],
        :month              => params[:month],
        :year               => params[:year],
        :first_name         => params[:first_name],
        :last_name          => params[:last_name]
      )
      response = STANDARD_GATEWAY.authorize(amount, credit_card, {:ip => request.remote_ip})
      if response.success?
        checkout.total_cart = checkout.total_cart + session[:params][:amount]

        purchase = STANDARD_GATEWAY.capture(amount, response.params["transaction_id"] )
        if purchase.success?
          checkout.status = "Changed"
          if session[:params][:type].eql?("no_room")
            no_room = session[:params][:no_room]
            night = checkout.nights
            checkout.no_room = no_room
          elsif session[:params][:type].eql?("move_date") || session[:params][:type].eql?("change_date")
            night = (session[:params][:check_out].to_date - session[:params][:check_in].to_date).to_i
            no_room = checkout.no_room
            checkout.check_in = session[:params][:check_in]
            checkout.check_out = session[:params][:check_out]
          end

          checkout.total_price = change_total_price(night, no_room, checkout.per_night)
          checkout.service = change_service(night, no_room, checkout.per_night)
          checkout.government = change_government(night, no_room, checkout.per_night)
          checkout.total_cart = change_total_cart(night, no_room, checkout.per_night)

          checkout.save

          ContactUs.change(checkout, session[:params]).deliver
          flash[:notice] = "Your checkout successfully changed."
          session[:params] = nil
        else
          flash[:notice] = 'Your checkout failed to change.'
        end
      else
        flash[:notice] = 'Your checkout failed to change.'
      end

      redirect_to dashboard_details_url(checkout)
    end
  end

  private

  def set_property
    id = params[:id] || params[:property_id]
    @property = Property.friendly.find(id)
  end

  def cancellation
    @checkout = Checkout.find(params[:id])
    unless @checkout.user_id.eql?(current_user.id)
      render "public/404"
    end

    if @checkout.status.eql?("Changed") || @checkout.status.eql?("Refunded")
      render "public/404"
    end

    @room = @checkout.room
    @property = @checkout.property

    @range = (@checkout.check_in - Date.today).to_i
    
    service = @checkout.service
    government = @checkout.government
    per_night = @checkout.per_night

    if @checkout.free_change?
      @refund = @checkout.total_cart - @checkout.service
    else
      @refund = @checkout.total_cart - (@checkout.service + @checkout.per_night)
    end
  end
end
