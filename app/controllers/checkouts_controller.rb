class CheckoutsController < ApplicationController
  include CheckoutsHelper
  before_action :set_checkout, only: :show
  load_and_authorize_resource param_method: :my_sanitizer

  def new
    unless params[:cart].nil?
      session[:cart] = params[:cart]
      session[:profile] = nil
    end
    if session[:cart].nil?
      render "public/404"
    else
      @room = Room.find(session[:cart][:room_id])
      @property = @room.property
      
      @months = [['Jan', 1], ['Feb', 2], ['Mar', 3], ['Apr', 4], ['Mei', 5], ['Jun', 6], ['Jul', 7], ['Aug', 8], ['Sep', 9], ['Oct', 10], ['Nov', 11], ['Dec', 12]]
      @years = Array(Time.now.year..(Time.now + 10.year).year)
      
      unless params[:user].eql?("guest")
        if !user_signed_in?
          render "checkouts/book_now"
        end
      end

    end
  end

  def create
    room = Room.find(session[:cart][:room_id].to_i)
    amount =  total_cart(session[:cart]) * 100
    
    if params[:checkout][:pay_type] == "card"
      credit_card = ActiveMerchant::Billing::CreditCard.new(card_params)
      options = {
        :ip => request.remote_ip,
        :email => params[:checkout][:email_id],
        :customer => params[:billing][:fullname],
        :address => {
          :name             => params[:billing][:fullname],
          :address1         => params[:billing][:address],
          :address2         => params[:billing][:address_two],
          :city             => params[:billing][:city],
          :state            => params[:billing][:state],
          :country          => params[:billing][:select_country],
          :zip              => params[:billing][:postal_code],
          :phone            => params[:billing][:mobile]
        }
      }
      response = STANDARD_GATEWAY.authorize(amount, credit_card, options)
      if response.success?
        @checkout = Checkout.new(checkout_create)
        @checkout.user_id = current_user.id if current_user
        @checkout.city           = params[:billing][:city]
        @checkout.address        = params[:billing][:address]
        @checkout.state          = params[:billing][:state]
        @checkout.select_country = params[:billing][:select_country]
        @checkout.postal_code    = params[:billing][:postal_code]
        @checkout.mobile         = params[:billing][:mobile]
        #save price
        @checkout.total_price    = total_price(session[:cart])
        @checkout.government     = government_tax(session[:cart])
        @checkout.service        = service_tax(session[:cart])
        @checkout.total_cart     = total_cart(session[:cart])
        @checkout.per_night      = session[:cart][:price].to_f

        @checkout.check_in       = session[:cart][:check_in]
        @checkout.check_out      = session[:cart][:check_out]
        @checkout.nights         = session[:cart][:night]
        @checkout.room_id        = session[:cart][:room_id]
        @checkout.no_room        = session[:cart][:no_room]
        @checkout.guest          = session[:cart][:guest]

        #save credit card
        @checkout.card_number = credit_card.display_number
        @checkout.brand = credit_card.brand
        @checkout.year = credit_card.year
        @checkout.month = credit_card.month
        

        @checkout.status = "Not Confirm"
        @checkout.authorization_id = response.params["transaction_id"]
        if @checkout.save
          if room.instant
            purchase = STANDARD_GATEWAY.capture(amount, @checkout.authorization_id )
            @checkout.transaction_id = purchase.params["transaction_id"]

            if purchase.success?
              @checkout.status = "Confirmed"
              @checkout.save

              pdf = WickedPdf.new.pdf_from_string(
                render_to_string('contact_us/voucher_hotel', :layout => false, :locals => {:checkout => @checkout})
              )
              ContactUs.instant_booking(@checkout, pdf).deliver
              ContactUs.submit_order_admin(@checkout).deliver
            else
              render "failed"
            end
          else
            ContactUs.submit_order_admin(@checkout).deliver
            ContactUs.submit_order_user(@checkout).deliver
          end

          #create activity
          @checkout.create_activity(:create, :owner => @checkout.property.user)

          session[:cart] = nil
          session[:profile] = nil
          render "success"
        else
          render "failed"
        end
      else
        session[:profile] = params[:checkout].merge(params[:billing])
        @message = response.message
        render "failed"
      end
      
    else
      session[:checkout] = params[:checkout]
      response = EXPRESS_GATEWAY.setup_purchase(amount,
        :ip                => request.remote_ip,
        :return_url        => checkouts_success_url,
        :cancel_return_url => root_url,
        :currency => "USD",
        :items => [{
            :description => "Paying for order",
            :amount => amount
          }]
      )
      redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
    end
    
  end

  def success
    if params[:token]
      if session[:checkout] && session[:cart]
        room = Room.find(session[:cart][:room_id].to_i)

        @checkout = Checkout.new
        @checkout.first_name  = session[:checkout][:first_name]
        @checkout.last_name   = session[:checkout][:last_name]
        @checkout.email_id    = session[:checkout][:email_id]
        @checkout.special     = session[:checkout][:special]
        #save price
        @checkout.total_price = total_price(session[:cart])
        @checkout.government  = government_tax(session[:cart])
        @checkout.service     = service_tax(session[:cart])
        @checkout.total_cart  = total_cart(session[:cart])
        @checkout.per_night   = session[:cart][:price].to_f
        #save detail booking
        @checkout.guest       = session[:cart][:guest]
        @checkout.check_in    = session[:cart][:check_in]
        @checkout.check_out   = session[:cart][:check_out]
        @checkout.room_id     = session[:cart][:room_id]
        @checkout.nights      = session[:cart][:night]
        @checkout.no_room     = session[:cart][:no_room]

        #save detail paypal
        @checkout.express_payer_id  = params[:PayerID]
        @checkout.express_token     = params[:token]
        
        @checkout.status            = "Not Confirm"
        @checkout.pay_type          = "Paypal"

        @checkout.user_id           = current_user.id if current_user
        
        if @checkout.save
          if room.instant
            purchase = EXPRESS_GATEWAY.purchase((total_cart(session[:cart]) * 100), 
                  :ip => request.ip,
                  :token => @checkout.express_token,
                  :payer_id => @checkout.express_payer_id
                )
            if purchase.success?
              @checkout.status = "Confirmed"
              @checkout.save

              pdf = WickedPdf.new.pdf_from_string(render_to_string('contact_us/voucher_hotel', :layout => false, :locals => {:checkout => @checkout}))
              ContactUs.instant_booking(@checkout, pdf).deliver
              ContactUs.submit_order_admin(@checkout).deliver
            end
          else
            ContactUs.submit_order_admin(@checkout).deliver
            ContactUs.submit_order_user(@checkout).deliver
          end

          @checkout.create_activity(:create, :owner => @checkout.property.user)
        else
          render "failed"
        end
        session[:cart] = nil
        session[:checkouts] = nil
      end

      @checkout = Checkout.find_by_express_token(params[:token])

    else
      redirect_to root_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def credit_card
      ActiveMerchant::Billing::CreditCard.new(
        :number             => params[:card][:card_number],
        :verification_value => params[:card][:cvv],
        :month              => params[:card][:month],
        :year               => params[:card][:year],
        :first_name         => params,
        :last_name          => last_name
      )
    end

    def set_checkout
      @checkout = Checkout.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def checkout_params
      params.require(:checkout).permit(:last_name, :custom_id, :first_name, :email_id, :special, :gender, :mobile, :country, :passport, :birth, :estimate, :card_number, :card_holders, :card_type, :cvv_code, :mounth, :year, :select_country, :address, :state, :city, :postal_code, :check_in, :check_out)
    end

    def checkout_create
      params.require(:checkout).permit(:first_name, :last_name, :email_id, :special, :general_price, :check_in, :check_out, :guest, :pay_type, :property_id, :nights)
    end

    def my_sanitizer
      params.require(:checkout).permit(:name)
    end

    def card_params
      params.require(:card).permit(:first_name, :last_name, :number, :verification_value, :month, :year)
    end
end
