class PrivacyAndSecurityStatementsController < ApplicationController
  before_action :set_privacy_and_security_statement, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /privacy_and_security_statements
  # GET /privacy_and_security_statements.json
  def index
    @privacy_and_security_statements = PrivacyAndSecurityStatement.all
  end

  # GET /privacy_and_security_statements/1
  # GET /privacy_and_security_statements/1.json
  def show
    @privacy_and_security_statements = PrivacyAndSecurityStatement.all
  end

  # GET /privacy_and_security_statements/new
  def new
    @privacy_and_security_statement = PrivacyAndSecurityStatement.new
  end

  # GET /privacy_and_security_statements/1/edit
  def edit
  end

  # POST /privacy_and_security_statements
  # POST /privacy_and_security_statements.json
  def create
    @privacy_and_security_statement = PrivacyAndSecurityStatement.new(privacy_and_security_statement_params)

    respond_to do |format|
      if @privacy_and_security_statement.save
        format.html { redirect_to @privacy_and_security_statement, notice: 'Privacy and security statement was successfully created.' }
        format.json { render action: 'show', status: :created, location: @privacy_and_security_statement }
      else
        format.html { render action: 'new' }
        format.json { render json: @privacy_and_security_statement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /privacy_and_security_statements/1
  # PATCH/PUT /privacy_and_security_statements/1.json
  def update
    respond_to do |format|
      if @privacy_and_security_statement.update(privacy_and_security_statement_params)
        format.html { redirect_to @privacy_and_security_statement, notice: 'Privacy and security statement was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @privacy_and_security_statement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /privacy_and_security_statements/1
  # DELETE /privacy_and_security_statements/1.json
  def destroy
    @privacy_and_security_statement.destroy
    respond_to do |format|
      format.html { redirect_to privacy_and_security_statements_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_privacy_and_security_statement
      @privacy_and_security_statement = PrivacyAndSecurityStatement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def privacy_and_security_statement_params
      params.require(:privacy_and_security_statement).permit(:title, :body)
    end

    def my_sanitizer
      params.require(:privacy_and_security_statement).permit(:name)
    end
end
