class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy, :map, :blog_cities]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /cities
  # GET /cities.json
  def index
    @city = City.all
    @cities = @city.joins(:properties).uniq
    @properties = Property.published.order(:created_at => :desc).includes(:photos).limit(3)

    @deals = RoomDate.available.available_date
    @page = Page.where(:name => 'homepage').first

    @select_cities = @city.map {|city| [city.name, city.id]}
    @guests = Array(1..15)

    @img = Homeimg.find_by(:active => true).image rescue "/assets/calendar-bg.jpg"
  end

  def cities_info
    @cities = City.order(:created_at => :desc)
  end

  def blog_guides
    @cities = City.all.page(params[:page]).per(15)
    @blogs = Blog.where(:blog_category_id => params[:id]).all
    @blogs_guides = Blog.all.page(params[:page]).per(3)
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:blog_id => params[:id])
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @popular_cities = City.joins(:places).order("places.id desc").limit(7)
    @city_first = City.joins(:places).order("places.id desc").limit(1)

    @hash = Gmaps4rails.build_markers(@cities) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def map
    @cities = City.where.not(:id => @city.id)
    @places = Place.where(:city_id => @city.id)
    @hash = []
    @places.each do |place|
      url = '<a href="'+city_place_url(@city, place)+'">'+place.title+'</a>'
      @hash << {:latlng => [place.lat, place.lng], :popup => url}
    end
    @panTo = []
    @places.map {|p| @panTo << [p.lat, p.lng]}
  end


  # GET /cities/1
  # GET /cities/1.json
  def show
    @cities = City.where("id <> ?", @city.id)
  end

  def blog_cities
    @blogs = Blog.where(:blog_category_id => params[:id]).all
    @blogs_all = Blog.all.page(params[:page]).per(3)
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:id => params[:id]).all
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @cities = City.all
    #@places = @city.places.page(params[:page]).per(15)

    @hash = Gmaps4rails.build_markers(@places) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def map_cities
    @blogs = Blog.where(:blog_category_id => params[:id]).all
    @blog_categories = BlogCategory.all
    @blog_comments = BlogComment.where(:id => params[:id])
    @popular_blogs = Blog.joins(:blog_comments).order("blog_comments.id desc").limit(3)
    @cities = City.all
    @places = @city.places.page(params[:page]).per(15)

    @hash = []
    @places.map {|p| @hash << {:latlng => [p.lat, p.lng]} }
    @panTo = []
    @places.map {|p| @panTo << [p.lat, p.lng]}
  end


  # GET /cities/admin
  def admin
    @cities = City.all
    @places = Place.all

    @hash = Gmaps4rails.build_markers(@cities) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def user_admin
    @cities = City.all
    @hotels = Hotel.all
    @places = Place.all

    @hash = Gmaps4rails.build_markers(@cities) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # GET /cities/new
  def new
    @city = City.new
  end

  # GET /cities/1/edit
  def edit
  end

  def order_sent
    @checkout = Checkout.find(session[:checkout_id])
  end

  # POST /cities
  # POST /cities.json
  def create
    #@city = current_user.cities.new(city_params)
    #@city.user_email = current_user.email
   @city = City.new(city_params)

    respond_to do |format|
      if @city.save
        format.html { redirect_to @city, notice: 'City was successfully created.' }
        format.json { render action: 'show', status: :created, location: @city }
      else
        format.html { render action: 'new' }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    if params[:delete_image]
      @city.remove_cityimg
    end
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to @city, notice: 'City was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to action: 'admin' }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:address, :name, :hotel_id, :description, :user_email, :place_id, :cityimg, :total, :density, :ethnicities, :religions, :country, :lat, :lng, :state, :town)
    end

    def my_sanitizer
      params.require(:city).permit(:name)
    end
end
