class CommentsController < ApplicationController
	before_action :authenticate_user!, only: :destroy
  load_and_authorize_resource param_method: :my_sanitizer

	def create
    @property = Property.friendly.find(params[:property_id])
	  @comment = @property.comments.create!(comment_params.merge(:checkout_id => session[:checkout_id]))
	  @comment.user_id = current_user.id
	  @comment.make_general_rating
	  if @comment.save
	  	@property.make_rating
	  	@property.rating = @comment.general_rating
	  	@property.save
	  	redirect_to property_reviews_path(@property), :notice => "Comment created!"
	  else
			redirect_to property_reviews_path(@property), :notice => "Error"
	  end
	  session[:checkout_id] = nil
	end

	def destroy
		comment = Comment.find(params[:id])
		property = comment.property
    if @comment.present?
      @comment.destroy
    end
    redirect_to property_reviews_path(property)
	end


	private

	  def comment_params
	    params.require(:comment).permit(:content, :atmosphere, :staff, :location, :value_for_money, :security, :cleanliness, :facilities)
	  end

	 	def my_sanitizer
      params.require(:comment).permit(:name)
    end
end
