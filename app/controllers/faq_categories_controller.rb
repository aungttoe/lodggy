class FaqCategoriesController < ApplicationController
  before_action :set_faq_category, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /faq_categories
  # GET /faq_categories.json
  def index
    @faq_categories = FaqCategory.all
  end

  # GET /faq_categories/1
  # GET /faq_categories/1.json
  def show
    @faq_posts = FaqPost.where(:faq_category_id => params[:id]).all
    @faq_categories = FaqCategory.all
  end

  # GET /faq_categories/new
  def new
    @faq_category = FaqCategory.new
  end

  # GET /faq_categories/1/edit
  def edit
  end

  # POST /faq_categories
  # POST /faq_categories.json
  def create
    @faq_category = FaqCategory.new(faq_category_params)

    respond_to do |format|
      if @faq_category.save
        format.html { redirect_to @faq_category, notice: 'Faq category was successfully created.' }
        format.json { render action: 'show', status: :created, location: @faq_category }
      else
        format.html { render action: 'new' }
        format.json { render json: @faq_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /faq_categories/1
  # PATCH/PUT /faq_categories/1.json
  def update
    respond_to do |format|
      if @faq_category.update(faq_category_params)
        format.html { redirect_to @faq_category, notice: 'Faq category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @faq_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /faq_categories/1
  # DELETE /faq_categories/1.json
  def destroy
    @faq_category.destroy
    respond_to do |format|
      format.html { redirect_to faq_categories_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_faq_category
      @faq_category = FaqCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def faq_category_params
      params.require(:faq_category).permit(:title, :faq_post_id)
    end

    def my_sanitizer
      params.require(:faq_category).permit(:name)
    end
end
