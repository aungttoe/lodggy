class RegistrationsController < Devise::RegistrationsController
  prepend_before_filter :require_no_authentication, only: [ :new, :cancel ]

  respond_to :js, :html
  def new
    redirect_to root_url  
  end

  def create
    if params[:user][:email].empty? || params[:user][:password].empty?
      if params[:user][:email].empty?
        flash[:notice] = "Email must be fill"
      else
        flash[:notice] = "Password must be fill"
      end
    else
  		build_resource(sign_up_params)
  		user = User.find_by_email(params[:user][:email].downcase)

      resource_saved = resource.save
      @user = resource

      if resource_saved
        flash[:notice] = :"Link confirmation sent to #{params[:user][:email]}"
      else
        clean_up_passwords resource
        @validatable = devise_mapping.validatable?
        if @validatable
          @minimum_password_length = resource_class.password_length.min
        end

        if !user.blank?
        	flash[:notice] = :"Email #{params[:user][:email].downcase} already exist"
        else
        	flash[:notice] = :"Password must be more than #{@minimum_password_length}" if params[:user][:password]
        end
      end
    end
	end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    resource_updated = update_resource(resource, account_update_params)
    yield resource if block_given?
    if resource_updated
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      sign_in resource_name, resource, bypass: true
      respond_with resource, location: after_update_path_for(resource)
    else
      @user = resource
      clean_up_passwords resource
      respond_to do |format|
        format.js
      end
    end
  end

  protected

    def after_update_path_for(resource)
      dashboard_root_url
    end

	  def after_sign_up_path_for(resource)
	  	ContactUs.user_sign_in(resource).deliver
	  	ContactUs.user_sign_in_to_admin(resource).deliver
	  end
    
end