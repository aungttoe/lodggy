class SessionsController < Devise::SessionsController
	def new
		redirect_to root_url
	end

	def create
		user = User.find_by_email(params[:user][:email])
		if user && user.valid_password?(params[:user][:password])
	  	self.resource = warden.authenticate!()
	    sign_in(resource_name, resource)
	    yield resource if block_given?
	  end
	  respond_to do |format|
	  	format.js
	  	format.html{ redirect_to :back }
	  end
  end

end