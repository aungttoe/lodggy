class HotelsController < ApplicationController
  before_filter :authenticate_user!, except: [:show, :h_map, :reviews]
  before_action :set_hotel, only: [:show, :edit, :update, :destroy, :previews, :h_map, :map_previews, :reviews, :reviews_hotel]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :set_city, only: [:show, :edit,:create, :update, :destroy, :previews, :h_map, :map_previews, :reviews, :reviews_hotel]
  before_action :check_hotel_published, only: [:show]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /hotels
  # GET /hotels.json
  def index
    @city = City.find(params[:city_id])    
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotels) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # GET /hotels/1
  # GET /hotels/1.json
  def show
    @city = City.find(params[:city_id])
    @comments_count = Comment.where(:hotel_id => @hotel.id)
    @hotel_comments = @hotel.comments.page(params[:page]).per(10)

    if params[:date_in]&&params[:date_out]
      @date_in = params[:date_in]
      @date_out = params[:date_out]
      @rooms = @hotel.filter_check_by_date(@hotel.id, @date_in, @date_out)
      @nights = @date_out.to_date-@date_in.to_date
      @check_in = @date_in
      @check_out = @date_out
      params[:date_now] =true
    elsif params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id).where("check_in_room >= ?", Time.now)
    end
    @room_types = RoomType.all
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def h_map    
    @city = City.find(params[:city_id])
    @comments_count = Comment.all

    
    if params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id)
    end
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def reviews    
    @city = City.find(params[:city_id])
    @comments_count = Comment.where(:hotel_id => @hotel.id)
    @hotel_comments = @hotel.comments.page(params[:page]).per(10)

    
    if params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id)
    end
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # GET /hotels/new
  def new
    @city = City.find(params[:city_id])
    @hotel = @city.hotels.new
    @property_type = ['Hostel', 'Hotel', 'Bed and Breakfast', 'Campsite', 'Apartment']
    @currency = ['Euro', 'US Dollar', 'British Pound', 'Albanian Lek', 'Algerian Dinar', 'Argentine Peso', 'Armenian Dram', 'Australian Dollar', 'CFP Franc', 'Canadian Dollar', 'Costa Rican Colon', 'Danish Krone', 'Honduran Lempira', 'Icelandic Krona', 'Indian Ruppe', 'Japanese Yen', 'Latvian Lats', 'Polish Zloty', 'Russian Rouble', 'Ukrainian Hryvnia']
    @language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @how = ['A friend', 'ITB', 'Kunio Koike', 'Melbourne Expo',  'Newspaper/Magazine Ad', 'News Article', 'Word of Mouth', 'Syndey Expo', 'Web Banner Ad', 'WYSTC', 'Channel Partner']
  end

  # GET /hotels/1/edit
  def edit
    @property_type = ['Hostel', 'Hotel', 'Bad and Breakfast', 'Campsite', 'Apartment']
    @currency = ['Euro', 'US Dollar', 'British Pound', 'Albanian Lek', 'Algerian Dinar', 'Argentine Peso', 'Armenian Dram', 'Australian Dollar', 'CFP Franc', 'Canadian Dollar', 'Costa Rican Colon', 'Danish Krone', 'Honduran Lempira', 'Icelandic Krona', 'Indian Ruppe', 'Japanese Yen', 'Latvian Lats', 'Polish Zloty', 'Russian Rouble', 'Ukrainian Hryvnia']
    @language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @how = ['A friend', 'ITB', 'Kunio Koike', 'Melbourne Expo',  'Newspaper/Magazine Ad', 'News Article', 'Word of Mouth', 'Syndey Expo', 'Web Banner Ad', 'WYSTC', 'Channel Partner']
  end

  def previews
    @city = City.find(params[:city_id])
    @comments_count = Comment.all
    @c = Comment.last
    @hotel.make_rating_params
    #@hotel.make_general_rating
    
    if params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id)
    end
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def reviews_hotel
    @city = City.find(params[:city_id])
    @comments_count = Comment.where(:hotel_id => @hotel.id)
    @c = Comment.last
    @hotel.make_rating_params
    #@hotel.make_general_rating
    
    if params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id)
    end
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  def map_previews
    @city = City.find(params[:city_id])
    @comments_count = Comment.all
    @c = Comment.last
    @hotel.make_rating_params
    #@hotel.make_general_rating
    
    if params[:check_in] || params[:check_out]
       @check_in = params[:check_in]
       @check_out = params[:check_out]
       @rooms = @hotel.filter_check_by_date(@hotel.id, @check_in, @check_out)
       @nights = @check_out.to_date-@check_in.to_date
    else
      @rooms = Room.where(hotel_id: @hotel.id)
    end
    @hotels = @city.hotels
    @hash = Gmaps4rails.build_markers(@hotel) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end

  # POST /hotels
  # POST /hotels.json
  def create
    @city = City.find(params[:city_id])
    @hotel = @city.hotels.new(hotel_params)
    @hotel.user_id = current_user.id
    @property_type = ['Hostel', 'Hotel', 'Bad and Breakfast', 'Campsite', 'Apartment']
    @currency = ['Euro', 'US Dollar', 'British Pound', 'Albanian Lek', 'Algerian Dinar', 'Argentine Peso', 'Armenian Dram', 'Australian Dollar', 'CFP Franc', 'Canadian Dollar', 'Costa Rican Colon', 'Danish Krone', 'Honduran Lempira', 'Icelandic Krona', 'Indian Ruppe', 'Japanese Yen', 'Latvian Lats', 'Polish Zloty', 'Russian Rouble', 'Ukrainian Hryvnia']
    @language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @how = ['A friend', 'ITB', 'Kunio Koike', 'Melbourne Expo',  'Newspaper/Magazine Ad', 'News Article', 'Word of Mouth', 'Syndey Expo', 'Web Banner Ad', 'WYSTC', 'Channel Partner']


    respond_to do |format|
      if @hotel.save
        if current_user.role == "admin"
          ContactUs.create_hotel_admin(@hotel, @current_user).deliver
        else
          ContactUs.create_hotel_admin(@hotel, @current_user).deliver
          ContactUs.create_hotel_user(@hotel, @current_user).deliver
        end

        if params[:images]
          params[:images].each { |image|
          @hotel.photos.create(image: image)
          }
        end
        format.html { render action: 'previews' }
        
      else
        format.html { render action: 'new' }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end

    

  end

  # PATCH/PUT /hotels/1
  # PATCH/PUT /hotels/1.json
  def update
    @property_type = ['Hostel', 'Hotel', 'Bad and Breakfast', 'Campsite', 'Apartment']
    @currency = ['Euro', 'US Dollar', 'British Pound', 'Albanian Lek', 'Algerian Dinar', 'Argentine Peso', 'Armenian Dram', 'Australian Dollar', 'CFP Franc', 'Canadian Dollar', 'Costa Rican Colon', 'Danish Krone', 'Honduran Lempira', 'Icelandic Krona', 'Indian Ruppe', 'Japanese Yen', 'Latvian Lats', 'Polish Zloty', 'Russian Rouble', 'Ukrainian Hryvnia']
    @language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @how = ['A friend', 'ITB', 'Kunio Koike', 'Melbourne Expo',  'Newspaper/Magazine Ad', 'News Article', 'Word of Mouth', 'Syndey Expo', 'Web Banner Ad', 'WYSTC', 'Channel Partner']


    @city = City.find(params[:city_id])
    respond_to do |format|
      if @hotel.update(hotel_params)

        if current_user.role == "admin"
          ContactUs.update_hotel_admin(@hotel, @current_user).deliver
        else
          ContactUs.update_hotel_admin(@hotel, @current_user).deliver
          ContactUs.update_hotel_user(@hotel, @current_user).deliver
        end

        photo_id = params[:remove] if params[:remove]
        
        if photo_id
          photo_id.each do |id|
            Photo.find(id).delete
            #@hotel.photos.delete (ph)
          end
        end
       
        if params[:images]
            params[:images].each { |image|
            @hotel.photos.create(image: image)
            }
          end
        
        format.html { render action: 'previews' }
      
       
      else
        format.html { render action: 'edit' }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end



  end

  # DELETE /hotels/1
  # DELETE /hotels/1.json
  def destroy
    @hotel_id = @hotel.id
    @hotel.destroy

    if current_user.role.eql?("admin")
      ContactUs.destroy_hotel_admin(@hotel, @current_user).deliver
    else
      ContactUs.destroy_hotel_admin(@hotel, @current_user).deliver
      ContactUs.destroy_hotel_user(@hotel, @current_user).deliver
    end

    respond_to do |format|
      @checkouts = Checkout.where('hotel_id' => @hotel_id).delete
      format.html { redirect_to hotels_index_path(@city) }
    end
  end


  def map
    @hotels = Hotel.where(published: true).all
    @hash = Gmaps4rails.build_markers(@hotels) do |user, marker|
      marker.lat user.lat
      marker.lng user.lng
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def check_hotel_published
      unless @hotel.published
        unless @hotel.user_id == current_user.id
          redirect_to root_url
        end
      end
    end

    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

    def set_user
      @current_user = current_user
    end

    def set_city
      @city = City.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hotel_params
      params.require(:hotel).permit(:title, :content, :private_room_price, :dorm_room_from, :distance_from_city_center, :rating_percent, :address, 
        :town, :state, :country, :lat, :lng, :room_id, :rating, :city_id, :all_property,:bed_and_breakfasts, :hostels, :hotels, :ensuite_room, 
        :single_room, :twin_room, :double_room, :triple_room, :family_room, :mixed_dorm, :male_dorm, :female_dorm, :breakfast_included, :internet_access, 
        :luggage_storage, :hour_check_in, :restaurant, :bar, :air_conditioning, :common_room, :travel_desk, :wheelchair_friendly, :all_payment, :credit_card, :debit_card, :paypal, 
        :address_two, :property_type, :total_number, :number_of_beds, :postcode, :contact_name, :manager_email, 
        :bookings_email, :website_address, :currency, :language, :how, :phone_number, :website,
        :description, :location, :directions, :things,
        :hour_reception, :card_phones, :gum, :nightclub, :hour_security, :celing_fan, :hairdryers, :outdoor_swimming, :adaptors, :cots_available,
        :hot_tub, :outdoor_terrace, :air_conditioning, :currency_exchange, :housekeeping, :parking, :airport_transfers, :direct_dial, :indoor_swimming, :postal_service,
        :atm, :dvds, :internet_access, :reading_light, :bar, :elevator, :key_card, :restaurant, :bicucle_hire, :fax_service,
        :landry_facilities, :safe_deposit, :book_exchenge, :free_airport, :lounge, :shuttle_bus, :breakfast_include, :free_city, :luggage_storage, :swimming_pool,
        :breakfast_not_include, :free_internet, :meeting_room, :making_facilities, :business_centre, :free_parking, :mini_bar, :cable_tv, :free_wifi, :mini_supermarket,
        :vending_mashines, :wifi, :age_restriction, :credit_card_not, :no_curfew, :taxes_included, :child_friendly, :curfew, :non_smoking, :taxes_not_included,
        :credit_cards_acc, :lock_out, :pet_friendly, 
        :published, :deals, :special_payment)
    end

    def my_sanitizer
      params.require(:hotel).permit(:name)
    end
end
