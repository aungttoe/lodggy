class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  before_action :set_hotel, only: [:show, :edit,:create, :update, :destroy]
  before_action :set_city, only: [:show, :edit,:create, :update, :destroy]  
  # load_and_authorize_resource param_method: :my_sanitizer

  # GET /rooms
  # GET /rooms.json
  def index
    @city = City.friendly.find(params[:city_id])
    @hotel = Hotel.find(params[:hotel_id])
    @rooms = @hotel.rooms
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
  end

  # GET /rooms/new
  def new
    @city = City.friendly.find(params[:city_id])
    @hotel = Hotel.find(params[:hotel_id])
    @room = @hotel.rooms.new
    @pay = ['Dorm', 'Private']
    @room_types = [['Single Room', 1], ['Twin Room',2], ['Double Room', 3], ['Triple Room',4], ['Family Room', 5], ['Mixed Dorm', 6], ['Male Dorm', 7], ['Female Dorm', 8]]
    # @room_types = ['Single Room', 'Twin Room', 'Double Room', 'Triple Room', 'Family Room', 'Mixed Dorm', 'Male Dorm', 'Female Dorm']
  end

  # GET /rooms/1/edit
  def edit
    @pay = ['Dorm', 'Private']
    @room_types = RoomType.all.map{|room_type| [room_type.name, room_type.id]}
  end

  # POST /rooms
  # POST /rooms.json
  def create
    @room = Room.new(room_params)
    @room.hotel_name = @hotel.title
    @room.city = @city.name

    respond_to do |format|
      if @room.save
        #@hotel.add_price
        @room.hotel.add_price
        format.html { redirect_to [@city,@hotel,@room], notice: 'Room was successfully created.' }
        format.json { render action: 'show', status: :created, location: @room }
      else
        format.html { render action: 'new' }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        @room.hotel.add_price
        format.html { redirect_to [@city,@hotel], notice: 'Room was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    @room.destroy
    @hotel.add_price
    respond_to do |format|
      format.html { redirect_to action: 'index' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    def set_hotel
      @hotel = Hotel.find(params[:hotel_id])
    end

    def set_city
      @city = City.friendly.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:title, :ensuite, :about, :price, :check_in_room, :check_out_room, :hotel_id, :count, :room_type_id, :guest, :pay_type, :photo)
    end

    def my_sanitizer
      params.require(:room).permit(:name)
    end
end
