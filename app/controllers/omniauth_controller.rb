class OmniauthController < Devise::OmniauthCallbacksController
	def all
		@user = User.from_omniauth(request.env["omniauth.auth"])
		if @user.confirmed?
			sign_in(@user)
			redirect_to dashboard_root_path
		else
			if @user.save
				flash[:notice] = "Success sign in"
				render "devise/shared/success"
			else
				flash[:notice] = "Fail to sign in"
				redirect_to root_url
			end
		end
	end
	alias_method :facebook, :all
	alias_method :google_oauth2, :all
end
