class PropertiesController < ApplicationController
  before_action :set_property, except: [:new, :create]
  before_action :is_published, only: [:show, :map, :reviews]
  before_action :set_variables, only: [:edit, :new]

  def show
  	@facilities = @property.facilities.where(:facility_type => "Facility")
  	@payments = @property.facilities.where(:facility_type => "Payment")
  	@policies = @property.facilities.where(:facility_type => "Policies")
  	@guests = Array(1..@property.guests)
    
    @check_in = params[:check_in]
    @check_out = params[:check_out]
    if @check_in && @check_out

      in_day, in_month, in_year = @check_in.split '/'
      out_day, out_month, out_year = @check_out.split '/'
      @valid_checkin = Date.valid_date?(in_year.to_i, in_month.to_i, in_day.to_i)
      @valid_checkout = Date.valid_date?(out_year.to_i, out_month.to_i, out_day.to_i)
      if @valid_checkout && @valid_checkin
        @room_dates = RoomDate.joins(:room).where("rooms.property_id = ? ", @property.id).where("? >= room_dates.from AND ? <= room_dates.to", @check_in.to_date, (@check_out.to_date + 1.day))
        @dates = @room_dates.where(:type_date => ['unavailable','booked'])

        @nights = (@check_out.to_date - @check_in.to_date).to_i
        @prices = @property.price * @nights
        @tax = (10 * @prices) / 100
        @fee = (5 * @prices) / 100
        @total = (@prices + @tax + @fee)
        @rooms = @property.rooms
      end
    end

  end

  def map
    @hash = [{:latlng => [@property.lat, @property.lng], :popup => @property.title}]
    @panTo = [@property.lat, @property.lng]
  end

  def reviews
    room_ids = @property.rooms.pluck(:id)
    if params[:booking_id]
      @checkout = Checkout.where(:booking_id => params[:booking_id]).first
      if @checkout.property.eql?(@property)
        if !@checkout.nil?
          session[:checkout_id] = @checkout.id
        end
      end
    end

    @all_comments = Comment.where(:property_id => @property.id).order(:created_at => :desc)
    @comments = Kaminari.paginate_array(@all_comments).page(params[:page]).per(10)
  end

  private

  def is_published
    unless @property.published
      render "public/404"
    end
  end

  def set_property
    id = params[:id] || params[:property_id]
  	@property = Property.friendly.find(id)
  end

  def set_variables
    @room_types = [["Entire Place"],["Private Room"],["Shared Room"]]
    @property_types = [["Hostel"], ["Bed and Breakfast"], ["Hotel"]]
    @currencies = [["EUR",'Euro'], ['US Dollar', 'USD']]
    @language = ['English', 'French', 'German', 'Spanish', 'Italian', 'Portuguese', 'Brazilian']
    @guests = Array(1..15)
    @cities = City.all.map {|city| [city.name, city.id]}
    @facilities = Facility.all
  end

  def property_params
    params.require(:property).permit(:address, :guests,  :address_two, :bedrooms, :city_id, :contact_name, :content, :currency, :description, :directions, :email, :language, :lat, :lng, :location, :phone, :postal_code, :price, :property_type, :room_type, :state, :things, :title, :website, {:facility_ids => []})
  end
end
