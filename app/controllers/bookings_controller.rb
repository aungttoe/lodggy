class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]
  before_action :set_room, only: [:show, :edit,:create, :update, :destroy]
  before_action :set_hotel, only: [:show, :edit,:create, :update, :destroy]
  before_action :set_city, only: [:show, :edit,:create, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  def index

    @city = City.friendly.find(params[:city_id])
    @hotel = Hotel.find(params[:hotel_id])
    @room = Room.find(params[:room_id])
    @bookings = @room.bookings
  end

  # GET /bookings/1
  # GET /bookings/1.json
  def show
  end

  # GET /bookings/new
  def new
    @city = City.friendly.find(params[:city_id])
    @hotel = Hotel.find(params[:hotel_id])
    @room = Room.find(params[:room_id])
    @booking = @room.bookings.new
  end

  # GET /bookings/1/edit
  def edit
  end

  # POST /bookings
  # POST /bookings.json
  def create
    @room = Room.find(params[:room_id])
    @booking = @room.bookings.new(booking_params)

    respond_to do |format|
      if @booking.save
        format.html { redirect_to [@city, @hotel, @room, @booking], notice: 'Booking was successfully created.' }
        format.json { render action: 'show', status: :created, location: [@city, @hotel, @room, @booking] }
      else
        format.html { render action: 'new' }
        format.json { render json: [@city, @hotel, @room, @booking].errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to [@city, @hotel, @room, @booking], notice: 'Booking was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    def set_room
      @room = Room.find(params[:room_id])
    end

    def set_hotel
      @hotel = Hotel.find(params[:hotel_id])
    end

    def set_city
      @city = City.friendly.find(params[:city_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:first_name, :last_name, :email)
    end
end
