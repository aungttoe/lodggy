class HomeimgsController < ApplicationController
  before_action :set_homeimg, only: [:show, :edit, :update, :destroy]
  load_and_authorize_resource param_method: :my_sanitizer

  # GET /homeimgs
  # GET /homeimgs.json
  def index
    @homeimgs = Homeimg.all
    @homeimg = Homeimg.last
  end

  # GET /homeimgs/1
  # GET /homeimgs/1.json
  def show
  end

  # GET /homeimgs/new
  def new
    @homeimg = Homeimg.new
  end

  # GET /homeimgs/1/edit
  def edit
  end

  # POST /homeimgs
  # POST /homeimgs.json
  def create
    @homeimg = Homeimg.new(homeimg_params)

    respond_to do |format|
      if @homeimg.save
        format.html { redirect_to homeimgs_url, notice: 'Homeimg was successfully created.' }
        format.json { render action: 'index', status: :created, location: @homeimg }
      else
        format.html { render action: 'new' }
        format.json { render json: @homeimg.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /homeimgs/1
  # PATCH/PUT /homeimgs/1.json
  def update
    respond_to do |format|
      if @homeimg.update(homeimg_params)
        format.html { redirect_to homeimgs_url, notice: 'Homeimg was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @homeimg.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /homeimgs/1
  # DELETE /homeimgs/1.json
  def destroy
    @homeimg.destroy
    respond_to do |format|
      format.html { redirect_to homeimgs_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homeimg
      @homeimg = Homeimg.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homeimg_params
      params.require(:homeimg).permit(:title, :image)
    end

    def my_sanitizer
      params.require(:homeimg).permit(:name)
    end
end
