class AdminController < ApplicationController
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
	before_filter :authenticate_user!
	before_filter :check_user, :notification

	layout "admin_dashboard"
  def check_user
    if current_user
    	if current_user.role.eql?("user")
    		render "public/404", layout: 'application'
    	end
    else
      render "public/404", layout: 'application'
    end
  end

  private
    def record_not_found
      render 'public/404_admin'
    end

  protected
  	def check_role
  		render("public/404_admin") if current_user.role.eql?("hotel")
  	end

  	def notification
	    if current_user.role.eql?("hotel")
	      @notifications = PublicActivity::Activity.where(:owner_id => current_user.id, :is_read_hotel => false)
	    else
	      @notifications = PublicActivity::Activity.where(:is_read_admin => false).order(:created_at => :desc)
	    end
	  end
end
