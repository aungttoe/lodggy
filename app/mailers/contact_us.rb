class ContactUs < ActionMailer::Base
  default from: "admin@mmlodging.com"

  def submit_contact_user(first, last, email, book, subject, message)
    @first = first
    @last = last
    @email = email
    @book = book
    @subject = subject
    @message = message
    mail(:to => @email, :subject => "Your contacts was send")
  end

  def submit_contact_admin(first, last, email, book, subject, message)
    @first = first
    @last = last
    @email = email
    @book = book
    @subject = subject
    @message = message
    mail(:to => 'admin.mmlodging@gmail.com', :subject => "You have new contact")
  end

  def submit_order_admin(checkout)
    @checkout = checkout
    mail(:to => checkout.property.email, :subject => "You Have New Checkout")
  end

  def submit_order_user(checkout)
    @checkout = checkout

    mail(:to => @checkout[:email_id], :subject => "Your Request Booking Was Sent")    
  end

  def confirm_order_admin(checkout)
    @checkout = checkout
    @rooms = @checkout.bookings.map {|booking| booking.room }
    mail(:to => 'admin.mmlodging@gmail.com', :subject => "Booking Confirmed")
  end

  def confirm_order_user(checkout, pdf)
    @checkout = checkout

    attachments["Hotel_Voucher_ID_#{checkout.booking_id}.pdf"] = pdf
    mail(:to => @checkout.email_id, :subject => "Request Booking Confirmed")
  end

  def create_hotel_admin(hotel, current_user)
    @current_user = current_user    
    @hotel = hotel    
    mail(:to => 'admin.mmlodging@gmail.com', :subject => "New hotel was create")
  end

  def create_hotel_user(hotel, current_user)
    @current_user = current_user    
    @hotel = hotel    
    mail(:to => @current_user.email, :subject => "Your hotel was create")
  end

  def update_hotel_admin(hotel, current_user)    
    @current_user = current_user
    @hotel = hotel
    mail(:to => 'admin.mmlodging@gmail.com', :subject => "The hotel was update")
  end

  def update_hotel_user(hotel, current_user)    
    @current_user = current_user
    @hotel = hotel
    mail(:to => @current_user.email, :subject => "The hotel was update")
  end

  def destroy_hotel_admin(hotel, current_user)    
    @current_user = current_user
    @hotel = hotel
    mail(:to => 'admin.mmlodging@gmail.com', :subject => "The hotel was destroy")    
  end

  def destroy_hotel_user(hotel, current_user)    
    @current_user = current_user
    @hotel = hotel
    mail(:to => @current_user.email, :subject => "The hotel was destroy")   
  end

  def user_sign_in(user)
    @user = user
    mail(:to => @user.email, :subject => "Your Account Already Confirmed")
  end

  def user_sign_in_to_admin(user)
    @user = user
    mail(:to => 'admin@mmlodging.com', :subject => "New User Registered")
  end

  def instant_booking(checkout, pdf)
    @checkout = checkout

    attachments["Hotel_Voucher_ID_#{checkout.booking_id}.pdf"] = pdf
    mail(:to => checkout.email_id, :subject => "Your Booking Has Been Confirmed")
  end

  def change(checkout, params)
    @checkout = checkout
    @params = params
    mail(:to => checkout.email_id, :subject => "Your Booking Has Changed")
  end

  def change_admin(checkout, params)
    @checkout = checkout
    @params = params
    mail(:to => checkout.property.email, :subject => "User Changed Booking")
  end

  def send_review(checkout)
    @checkout = checkout
    mail(:to => checkout.email_id, :subject => "Review #{@checkout.property.title}")
  end

  def cancel(checkout)
    @checkout = checkout
    mail(:to => checkout.email_id, :subject => "Your Booking Cancelled")
  end

  def cancel_admin(checkout)
    @checkout = checkout
    mail(:to => checkout.email_id, :subject => "User Booking Cancelled")
  end
end
