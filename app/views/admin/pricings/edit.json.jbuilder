json.extract! @dates, :id, :type_date, :no_room, :room_id, :note, :price, :checkout_id, :created_at, :updated_at
json.max @max
json.to @to
json.from @from