json.array!(@admin_homeimgs) do |admin_homeimg|
  json.extract! admin_homeimg, :id
  json.url admin_homeimg_url(admin_homeimg, format: :json)
end
