json.array!(@admin_faqs) do |admin_faq|
  json.extract! admin_faq, :id
  json.url admin_faq_url(admin_faq, format: :json)
end
