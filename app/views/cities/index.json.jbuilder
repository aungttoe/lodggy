json.array!(@cities) do |city|
  json.extract! city, :id, :name, :hotel_id, :description
  json.url city_url(city, format: :json)
end
