json.array!(@faq_posts) do |faq_post|
  json.extract! faq_post, :id, :title, :content
  json.url faq_post_url(faq_post, format: :json)
end
