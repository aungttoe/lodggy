json.array!(@privacy_and_security_statements) do |privacy_and_security_statement|
  json.extract! privacy_and_security_statement, :id, :title, :body
  json.url privacy_and_security_statement_url(privacy_and_security_statement, format: :json)
end
