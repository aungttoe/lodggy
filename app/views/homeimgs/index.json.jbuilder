json.array!(@homeimgs) do |homeimg|
  json.extract! homeimg, :id, :title
  json.url homeimg_url(homeimg, format: :json)
end
