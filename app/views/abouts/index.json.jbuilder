json.array!(@abouts) do |about|
  json.extract! about, :id, :column_1, :column_2, :column_3
  json.url about_url(about, format: :json)
end
