json.array!(@rooms) do |room|
  json.extract! room, :id, :type, :price, :check_in_room, :check_out_room
  json.url room_url(room, format: :json)
end
