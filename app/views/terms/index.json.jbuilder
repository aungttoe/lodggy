json.array!(@terms) do |term|
  json.extract! term, :id, :content
  json.url term_url(term, format: :json)
end
