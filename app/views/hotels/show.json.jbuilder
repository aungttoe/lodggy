json.extract! @hotel, :id, :title, :content, :private_room_price, :dorm_room_from, :distance_from_city_center, :rating_percent, :created_at, :updated_at
