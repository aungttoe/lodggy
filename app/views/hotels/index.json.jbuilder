json.array!(@hotels) do |hotel|
  json.extract! hotel, :id, :title, :content, :private_room_price, :dorm_room_from, :distance_from_city_center, :rating_percent
  json.url hotel_url(hotel, format: :json)
end
