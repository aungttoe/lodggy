json.array!(@bookings) do |booking|
  json.extract! booking, :id, :first_name, :last_name, :email
  json.url booking_url(booking, format: :json)
end
