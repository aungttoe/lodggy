json.array!(@checkouts) do |checkout|
  json.extract! checkout, :id, :last_name, :first_name, :email_id, :special, :gender, :mobile, :country, :birth, :estimate, :card_number, :card_holders, :card_type, :cvv_code, :mounth, :year, :select_country, :address, :state, :city, :postal_code
  json.url checkout_url(checkout, format: :json)
end
