// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require admin_dashboard/fullcalendar/moment.min
//= require jquery
//= require jquery_ujs
//= require foundation
//= require underscore
//= require gmaps/google
//= require redactor-rails
//= require jquery.sticky.js

//= require jcarousel.connected-carousels
//= require imagemanager
//= require jquery-ui-1.10.4.min
//= require jquery.geocomplete.min
//= require jquery.jcarousel.min
//= require jquery.raty
//= require lightbox.min
//= require modernizr
//= require parsley.min
//= require jquery.payment
//= require table
//= require app
//= require ahoy

ahoy.trackView();

$(function(){ $(document).foundation(); });


$(function() {
  /*
  $("#products th a, #products .pagination a").on("click", function() {
    $.getScript(this.href);
    return false;
  });
  */

$("#searchform input").keyup(function() {
    $.get($("#searchform").attr("action"), $("#searchform").serialize(), null, "script");
    return false;
  });

$("#searchform input").change(function() {
    $.get($("#searchform").attr("action"), $("#searchform").serialize(), null, "script");
    return false;
  });

$("#searchform select").change(function() {
    $.get($("#searchform").attr("action"), $("#searchform").serialize(), null, "script");
    return false;
  });
});

var score_get = function() {
    return ($(this).attr('data-rating'));
  };
$('.rating_star').raty({
  score: score_get,
  readOnly: true
});

$(".filter_rating_from").raty({
  score: score_get,
  half: true,
  click: function(score, evt){
    $("#from_rating").val(score * 100 / 5);
  }
});

$(".filter_rating_to").raty({
  score: score_get,
  half: true,
  click: function(score, evt){
    $("#to_rating").val(score * 100 / 5);
  }
});
$(document).ready(function(){
  $("#sticky-div").sticky({
    topSpacing: 0,
    bottomSpacing: 350,
    responsiveWidth: false
  });

  $("#credit_card").click(function(){
    $("#pay_type").val("card");
    $("#checkout_form").parsley().destroy();
    $('.input-require').attr("required", true);
    $("#checkout_form").parsley();
    $("#checkout_form").attr("");
  });

  $("#paypal").click(function(){
    $("#pay_type").val("paypal");
    $("#checkout_form").parsley().destroy();
    $('.input-require').removeAttr("required");
    $("#checkout_form").parsley();
  });

  $("#card_number").payment('formatCardNumber');
  $("#card_verification_value").payment('formatCardCVC');

});