$(document).ready(function () {
  $('.select_rooms').change(function(){
    var id = $(this).attr('room_id');
    var no = $(this).val();
    var price = $(this).parent().attr('price');
    var night = parseInt($("#checkout_night").val());
    var check_in = $('#checkout_check_in').val();
    var check_out = $('#checkout_check_out').val();
    var title = $('#title_'+id).html();
    
    total_price = parseFloat(price * night * no);

    price = parseFloat(price).toFixed(2);
    if(no == 0){
      $('#room_id_'+id).remove();
    }else{
      $('#room_id_'+id).remove();
      $('#content').append('<tr id="room_id_'+id+'"><td>'+title+'</td><td>'+check_in+'</td><td>'+check_out+'</td><td>'+night+'</td><td>$'+price+'</td><td><span class="room_price" price="'+total_price+'">$'+total_price.toFixed(2)+'</span></td></tr>');
      $('#rooms').append('<input type="hidden" name="booking[room_id][]" value="'+ id +'">');
      $('#no_rooms').append('<input type="hidden" name="booking[no_room][]" value="'+ no +'">');
    };
        
    total = 0
    $(".room_price").each(function(){
     total = total + parseInt($(this).attr('price'));
    });
    $('#checkout_total_price').val(total);
    total = parseFloat(total).toFixed(2);
    $('#count_price').html('$'+total);
  });
  // $("#firsttable .numberOfGuestsOrRooms select").change(function(){
  //   var total = 0;
  //   var total_price_room = 0;
  //   var guest = $(this).val();
  //   var room_id = $(this).parent().parent().attr("id");
  //   var price = parseInt($("#"+room_id+" .hotelPrice span.hide").html());
  //   var room_name = $("#"+room_id+" .hotelName .name").html();
  //   var nights = parseInt($("#nights").val());

  //   total_price_room = ((nights * price) * guest).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  //   var content_table = "<tr id='room_"+room_id+"'><td>"+room_name+"</td> <td>"+guest+"</td> <td>"+nights+"</td> <td>$ <span class='room_price'>"+total_price_room+"</span></td></tr>";

  //   var guest_field = $("#" + room_id + " .numberOfGuestsOrRooms select");
  //   var price_field = $("#" + room_id + " .hotelPrice .hide input");
  //   var room_field = $("#" + room_id + " .hotelName .hide input");
    
  //   $("#room_"+room_id).remove();
  //   if(guest != 0){
  //     $("#secondtable table tbody").append(content_table);
  //     $(guest_field).attr("name", "field_guest[guest][]");
  //     $(price_field).attr("name", "field_price[price][]");
  //     $(room_field).attr("name", "field_type[type][]");
  //   }else{
  //     $(guest_field).attr("name", "");
  //     $(price_field).attr("name", "");
  //     $(room_field).attr("name", "");
  //   }


  //   $(".room_price").each(function(){
  //    total = total + parseInt($(this).html());
  //   })

  //   if($("#pay").length == 0){
  //     $(".deposit-button").append('<input class="button-full-width-orange" id="pay" type="submit" value="Book now">');
  //   }
  //   $("#totalPrice").html("$ "+total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
  //   $("#total_price").val(total);
  // });
});