//= require jquery_ujs
//= require jquery-migrate-min
//= require jquery-ui
//= require bootstrap

//= require underscore
//= require gmaps/google

//= require jquery.slimscroll.min
//= require admin_dashboard/jquery.blockui.min
//= require admin_dashboard/jquery.cokie.min
//= require admin_dashboard/jquery.uniform.min


//= require admin_dashboard/js/jquery.validate.min
//= require admin_dashboard/js/additional-methods.min
//= require admin_dashboard/jquery.bootstrap.wizard
//= require admin_dashboard/select2/select2.min
//= require admin_dashboard/icheck/icheck.min
//= require admin_dashboard/form-icheck
//= require admin_dashboard/bootstrap-fileinput
//= require admin_dashboard/bootstrap-datepicker/bootstrap-datepicker
//= require admin_dashboard/bootstrap-hover-dropdown.min
//= require ckeditor/init
//= require admin_dashboard/fullcalendar/fullcalendar.min
//= require admin_dashboard/touchspin
//= require jquery.tagsinput

//= require admin_dashboard/metronic
//= require admin_dashboard/layout
//= require admin_dashboard/demo
//= require admin_dashboard/form-wizard

//= require toastr
//= require highcharts/highcharts

$("#btn_new_image").click(function() {
  $("#new_images_path tbody").append("<tr><td></td><td><div class=\"fileinput fileinput-new\" data-provides=\"fileinput\"><div class=\"input-group input-large\"><div class=\"form-control uneditable-input span3\" data-trigger=\"fileinput\"><i class=\"fa fa-file fileinput-exists\"></i>&nbsp; <span class=\"fileinput-filename\"></span></div><span class=\"input-group-addon btn default btn-file\"><span class=\"fileinput-new\">Select file </span><span class=\"fileinput-exists\">Change </span><input type=\"file\" name=\"images[]\"></span><a href=\"javascript:;\" class=\"input-group-addon btn red fileinput-exists\" data-dismiss=\"fileinput\">Remove </a></div></div></td></tr>");
});
$(".date-picker").datepicker({
  format: "yyyy-mm-dd",
  startDate: "0d"
});
$("[date-toggle='popover']").popover({
  html: true
});
({
  trigger: "hover"
});
$('[data-toggle="tooltip"]').tooltip();

$("#refund_amount").on('show.bs.modal', function(e){
	var button = $(e.relatedTarget);
	var total_price = button.data("amount");
	var id = button.data("checkout-id");
	console.log(id);
	refund = (total_price / 115 * 100);
	var modal = $(this);
	modal.find(".total_amount").val(total_price);
	modal.find(".amount_refund").val(refund);
	modal.find(".checkout_id").val(id);
});

$('.tags').tagsInput({
  'width': '100%'
});