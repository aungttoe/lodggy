$(function() {
    $( "#slider-range1" ).slider({
        range: true,
        min: 0,
        max: 200,
        values: [ 0, 200 ],
        slide: function( event, ui ) {
            $( "#amount1" ).val( "$" + ui.values[ 0 ] + " to $" + ui.values[ 1 ] );
            $( "#amount201" ).val(ui.values[ 0 ]);
            $( "#amount202" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount1" ).val( "$" + $( "#slider-range1" ).slider( "values", 0 ) +
        " to $" + $( "#slider-range1" ).slider( "values", 1 ) );
    $( "#amount201" ).val($( "#slider-range1" ).slider( "values", 0 ));
    $( "#amount202" ).val($( "#slider-range1" ).slider( "values", 1 ));

    $( "#slider-range2" ).slider({
        range: true,
        min: 0,
        max: 100,
        values: [ 0, 100 ],
        slide: function( event, ui ) {
            $( "#amount2" ).val(ui.values[ 0 ] + " to " + ui.values[ 1 ] );
            $( "#amount21" ).val(ui.values[ 0 ]);
            $( "#amount22" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount2" ).val( $( "#slider-range2" ).slider( "values", 0 ) +
        " to " + $( "#slider-range2" ).slider( "values", 1 ) );
    $( "#amount21" ).val($( "#slider-range2" ).slider( "values", 0 ));
    $( "#amount22" ).val($( "#slider-range2" ).slider( "values", 1 ));
});

/*DATAPICKER*/

$(function() {
    var dateToday = new Date();
    var dates = $("#from, #to").datepicker({
        dateFormat: 'dd/mm/yy',
        minDate: dateToday,
        onSelect: function(selectedDate) {
            var option = this.id == "from" ? "minDate" : "maxDate",
                instance = $(this).data("datepicker"),
                date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
            dates.not(this).datepicker("option", option, date);
        }
    });
});

$("#from, #to").keydown(function(event) {
    if (event.keyCode != 9) {
        return false;
    }
});



$( "#from, #to" ).click(function() {
    var height = $(this).outerHeight();
    var left = $(this).offset().left;
    var top = $(this).offset().top + height;
    $("#ui-datepicker-div").css({"left": left, "top": top});
});


$( ".datepicker-center" ).click(function() {
    var height = $(this).outerHeight();
    var width =  $("#ui-datepicker-div").width()/2 - $(this).width()/2;
    var left = $(this).offset().left - width+8;
    var top = $(this).offset().top + height;
    $("#ui-datepicker-div").css({"left": left, "top": top});
});


$.extend($.datepicker,{_checkOffset:function(inst,offset,isFixed){return offset}});

/*END-DATAPICKER*/

/*ACCORDION*/

$(function() {
    var $accordionIO = $('.accordion-block h3');
    $accordionIO.next('div').hide();

    $accordionIO.click(function(){
        windowWidth = $( window ).width();
        if( windowWidth  < 768 ) {
            $(this).next('div').toggle();
        }
        else {
            $(this).next('div').slideToggle("fast");
        }
    });
});

$( ".accordion-block h3" ).click(function() {
    if ( $(this).hasClass("active") ) {
        $(this).removeClass("active");
    }
    else {
        $(this).addClass("active");
    }
});


/*END-ACCORDION*/


// $( ".header_sign-in-out" ).click(function() {
//     $(".first-header").css("display","none");
//     $(".second-header").css("display","block");
// });

$( ".header_forgottpass_quit" ).click(function() {
    $(".first-header").css("display","block");
    $(".second-header").css("display","none");
});


/*OFFSET  MENU*/

/*! Pushy - v0.9.1 - 2013-9-16
 * Pushy is a responsive off-canvas navigation menu using CSS transforms & transitions.
 * https://github.com/christophery/pushy/
 * by Christopher Yee */$(function(){function h(){t.toggleClass(o);e.toggleClass(s);n.toggleClass(u);r.toggleClass(a)}function p(){t.addClass(o);e.animate({left:"0px"},l);n.animate({left:c},l);r.animate({left:c},l)}function d(){t.removeClass(o);e.animate({left:"-"+c},l);n.animate({left:"0px"},l);r.animate({left:"0px"},l)}var e=$(".pushy"),t=$("body"),n=$("#container"),r=$(".push"),i=$(".site-overlay"),s="pushy-left pushy-open",o="pushy-active",u="container-push",a="push-push",f=$(".menu-btn, .pushy a"),l=200,c=e.width()+"px";if(Modernizr.csstransforms3d){f.click(function(){h()});i.click(function(){h()})}else{e.css({left:"-"+c});n.css({"overflow-x":"hidden"});var v=!0;f.click(function(){if(v){p();v=!1}else{d();v=!0}});i.click(function(){if(v){p();v=!1}else{d();v=!0}})}});

$( document ).ready(function() {
    function offsetmenu() {
        windowWidth = $( window ).width();
        if( windowWidth  <  768) {
            leftblockHeight = $('.pushy').outerHeight();
            rightblockHeight = $('#container').outerHeight();
            if( leftblockHeight  > rightblockHeight ) {
                $(".content").css("height", leftblockHeight);
            }
            else {
                $(".content").css("height", rightblockHeight);
            }
        }
        else {
            $(".content").css("height","auto");
        }

    }
    function pushyadd() {
        windowWidth = $( window ).width();
        if( windowWidth  < 768 ) {
           if ( $("head").find("#removecss").length) {

           }
           else {
             $('head').append('<style id="removecss" type="text/css">' +
                            '.pushy {' +
                              'position: absolute;' +
                              'width: 50%;' +
                              'top: 0;' +
                              'z-index: 100;' +
                              'overflow: auto;' +
                              '-webkit-overflow-scrolling: touch;' +
                              '/* enables momentum scrolling in iOS overflow elements */' +
                            '}' +
                            '/* Menu Movement */' +
                            '.pushy-left {' +
                              '-webkit-transform: translate3d(-150%, 0, 0);' +
                              '-moz-transform: translate3d(-150%, 0, 0);' +
                              '-ms-transform: translate3d(-150%, 0, 0);' +
                              '-o-transform: translate3d(-150%, 0, 0);' +
                              'transform: translate3d(-150%, 0, 0);' +
                            '}' +
                            '.pushy-open {' +
                              '-webkit-transform: translate3d(0, 0, 0);' +
                              '-moz-transform: translate3d(0, 0, 0);' +
                              '-ms-transform: translate3d(0, 0, 0);' +
                              '-o-transform: translate3d(0, 0, 0);' +
                              'transform: translate3d(0, 0, 0);' +
                                'z-index: 99!important;' +
                            '}' +
                            '.container-push, .push-push {' +
                              '-webkit-transform: translate3d(55%, 0, 0);' +
                              '-moz-transform: translate3d(55%, 0, 0);' +
                              '-ms-transform: translate3d(55%, 0, 0);' +
                              '-o-transform: translate3d(55%, 0, 0);' +
                              'transform: translate3d(55%, 0, 0);' +
                            '}' +
                            '/* Menu Transitions */' +
                            '.pushy, #container, .push {' +
                              '-webkit-transition: -webkit-transform 0.2s cubic-bezier(0.16, 0.68, 0.43, 0.99);' +
                              '-moz-transition: -moz-transform 0.2s cubic-bezier(0.16, 0.68, 0.43, 0.99);' +
                              '-o-transition: -o-transform 0.2s cubic-bezier(0.16, 0.68, 0.43, 0.99);' +
                              'transition: transform 0.2s cubic-bezier(0.16, 0.68, 0.43, 0.99);' +
                              '/* improves performance issues on mobile*/' +
                              '-webkit-backface-visibility: hidden;' +
                              '-webkit-perspective: 1000;' +
                              'z-index: 9;' +
                              'background: #fff;' +
                            '}' +
                            '/* Site Overlay */' +
                            '.site-overlay {' +
                              'display: none;' +
                            '}' +
                            '.pushy-active .site-overlay {' +
                              'display: block;' +
                              'position: fixed;' +
                              'top: 0;' +
                              'right: 0;' +
                              'bottom: 0;' +
                              'left: 50%;' +
                              'z-index: 98;' +
                            '}' +
                            '@media (max-width: 568px) {' +
                                '.pushy {' +
                                    'position: absolute;' +
                                    'width: 70%;' +
                                    'top: 0;' +
                                    'z-index: 100;' +
                                    'overflow: auto;' +
                                    '-webkit-overflow-scrolling: touch;' +
                                    '/* enables momentum scrolling in iOS overflow elements */' +
                                '}' +
                                '.container-push, .push-push {' +
                                    '-webkit-transform: translate3d(80%, 0, 0);' +
                                    '-moz-transform: translate3d(80%, 0, 0);' +
                                    '-ms-transform: translate3d(80%, 0, 0);' +
                                    '-o-transform: translate3d(80%, 0, 0);' +
                                    'transform: translate3d(80%, 0, 0);' +
                                '}' +
                            '}' +
                            '</style>');
           }
            if ( $(".content").find(".pushy-left").length) {
                $(".content").css("height", rightblockHeight);
            }

        }
        else {
            $("#removecss").remove();
        }
    }
    $('body').bind('click', function(){
        windowWidth = $( window ).width();
        if( windowWidth  < 768 ) {
            if ( $(".content").find(".pushy-left").length) {
                $(".content").css("height", rightblockHeight);
            }
            if ( $(".content").find(".pushy-open").length) {
                offsetmenu();
            }
        }
        else {
            if ( $(".content").find(".pushy-open").length) {
                offsetmenu();
            }
        }
    });
    $( ".accordion1 h3" ).click(function() {
            offsetmenu();

    });
    offsetmenu();
    pushyadd();
    $( window ).resize(function() {
        offsetmenu();
        pushyadd();
    });


    var i = 0;
    $("#all_facilities").click(function(){
        if ($("#all_facilities").is(':checked')){
            $(".all_facilities input").attr('checked', true);
        }else{
            $(".all_facilities input").attr('checked', false);
        }
    });

    $("#all_payments").click(function(){
        if ($("#all_payments").is(':checked')){
            $(".all_payments input").attr('checked', true);
        }else{
            $(".all_payments input").attr('checked', false);
        }
    });

    $("#all_policies").click(function(){
        if ($("#all_policies").is(':checked')){
            $(".all_policies input").attr('checked', true);
        }else{
            $(".all_policies input").attr('checked', false);
        }
    });


    if ($(".date_now").hasClass("order") == true){
        //alert("lol");
        $(".show-form-date").removeClass('hide');
    }


});


var SITE = SITE || {};

SITE.fileInputs = function() {
    var $this = $(this);
    $val = $this.val();
    var valArray = $val.split("/");
    newVal = valArray[valArray.length-1];
    $button = $this.siblings('.button');
    $fakeFile = $this.siblings(".file-holder");
    if( newVal !== '') {
    $button.text('File uploaded');
    if($fakeFile.length === 0) {
    $button.after('<span class="file-holder">' + newVal + '</span>');
    $(this).closest(".input-file-block").css({"background": "url(/assets/input-file-bg.png)", "background-repeat": "no-repeat", "background-position": "98%"})
    }
    else {
    $fakeFile.text(newVal);
    }
    }
    };

$(document).ready(function() {
    $('.input-file-block input[type=file]').bind('change focus click', SITE.fileInputs);
    $(".gogogo").click(function(){
        if  ($(".i_accept").hasClass("hide") == true){
            $(".i_accept").removeClass("hide");
        }
        else {
            $(".i_accept").addClass("hide");
        }
    });
});


/*

// registration I ACCEPT


$(document).ready(function() {

      var valid = false
      $( ".title" )
      .focusout(function() {
        if ($(".title input").val() == ''){
          $(".title_over").removeClass('hide');
        }
        else {
          $(".title_over").addClass('hide');
        } 
      })

      $( ".type" )
      .focusout(function() {
        var str = document.getElementById("type").value
        if (str.options[str.selectedIndex].defaultSelected){
          $(".type_over").removeClass('hide');
          alert('lol');
        }
        else {
          $(".type_over").addClass('hide');
          alert('ok');
        } 
      })

      $( ".total_number" )
      .focusout(function() {
        var str = document.getElementById("total_number").value
        if (str < 1 || str > 10000 || str == ''){
          $(".total_number_over").removeClass('hide');
        }
        else {
          $(".total_number_over").addClass('hide');
        } 
      })

      $( ".number_of_beds" )
      .focusout(function() {
        var str = document.getElementById("number_of_beds").value
        if (str < 1 || str > 10000 || str == ''){
          $(".number_of_beds_over").removeClass('hide');
        }
        else {
          $(".number_of_beds_over").addClass('hide');
        } 
      })

      $( ".address" )
      .focusout(function() {
        if ($(".address input").val() == ''){
          $(".address_over").removeClass('hide');
        }
        else {
          $(".address_over").addClass('hide');
        } 
      })

      $( ".contact_name" )
      .focusout(function() {
        if ($(".contact_name input").val() == ''){
          $(".contact_name_over").removeClass('hide');
        }
        else {
          $(".contact_name_over").addClass('hide');
        } 
      })

      $( ".manager_email" )
      .focusout(function() {
        var str = document.getElementById("manager_email").value
        var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
        if (!str.match(re)){
          $(".manager_email_over").removeClass('hide');
          //alert("lol");
        }
        else {
          $(".manager_email_over").addClass('hide');
          //alert('ok');
        } 
      })

      $( ".bookings_email" )
      .focusout(function() {
        var str = document.getElementById("bookings_email").value
        var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
        if (!str.match(re)){
          $(".bookings_email_over").removeClass('hide');
          //alert("lol");
        }
        else {
          $(".bookings_email_over").addClass('hide');
          //alert('ok');
        } 
      })

      $( ".phone_number" )
      .focusout(function() {
        var str = document.getElementById("phone_number").value
        var phoneno = /^\d{10}$/;
        if (!str.match(phoneno)){
          $(".phone_number_over").removeClass('hide');
          //alert("lol");
        }
        else {
          $(".phone_number_over").addClass('hide');
          //alert('ok');
        } 
      })

      $( "#content" )
      .focusout(function() {
        var str_all = document.getElementById("content").value
        var str = str_all.length
        if (str < 150 || str > 300 || str == ''){
          $(".content_over").removeClass('hide');
        }
        else {
          $(".content_over").addClass('hide');
        } 
      })

      $( "#description" )
      .focusout(function() {
        var str_all = document.getElementById("description").value
        var str = str_all.length
        if (str < 500 || str > 1000 || str == ''){
          $(".description_over").removeClass('hide');
        }
        else {
          $(".description_over").addClass('hide');
        } 
      })

      $( "#location" )
      .focusout(function() {
        var str_all = document.getElementById("location").value
        var str = str_all.length
        if (str < 200 || str > 500 || str == ''){
          $(".location_over").removeClass('hide');
        }
        else {
          $(".location_over").addClass('hide');
        } 
      })

      $( "#directions" )
      .focusout(function() {
        var str_all = document.getElementById("directions").value
        var str = str_all.length
        if (str < 150 || str > 300 || str == ''){
          $(".directions_over").removeClass('hide');
        }
        else {
          $(".directions_over").addClass('hide');
        } 
      })

      $( "#things" )
      .focusout(function() {
        var str_all = document.getElementById("things").value
        var str = str_all.length
        if (str < 200 || str > 500 || str == ''){
          $(".things_over").removeClass('hide');
        }
        else {
          $(".things_over").addClass('hide');
        } 
      })






    });



function validatezip(inputText1) {

    var dateformat = /^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$/;
    // Match the date format through regular expression
    if(inputText1.value.match(dateformat))
    {
        //alert("lolololol");
        document.form1.Postcode.focus();
        //Test which seperator is used '/' or '-'
        var opera1 = inputText1.value.split('/');
        var opera2 = inputText1.value.split('-');
        lopera1 = opera1.length;
        lopera2 = opera2.length;
        // Extract the string into month, date and year
        if (lopera1>1)
        {
            var pdate = inputText1.value.split('/');
        }
        else if (lopera2>1)
        {
            var pdate = inputText1.value.split('-');
        }
        var dd = parseInt(pdate[0]);
        var mm  = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
        if (mm==1 || mm>2)
        {
            if (dd>ListofDays[mm-1])
            {
                //alert('Invalid date format0!');
                $(".check_in input").val('');
                $(".check_out input").val('');
                return false;
            }
        }
        if (mm==2)
        {
            var lyear = false;
            if ( (!(yy % 4) && yy % 100) || !(yy % 400))
            {
                lyear = true;
            }
            if ((lyear==false) && (dd>=29))
            {
                //alert('Invalid date format1!');
                $(".check_in input").val('');
                $(".check_out input").val('');
                return false;
            }
            if ((lyear==true) && (dd>29))
            {
                //alert('Invalid date format2!');
                $(".check_in input").val('');
                $(".check_out input").val('');
                return false;
            }
        }
    }
    else
    {
        //alert("Invalid date format3!");
        $(".postcode input").val('');
        document.form1.Postcode.focus();
        return false;
    }
}

*/

// validation calendar
function validatecal(inputText1, inputText2)  {

    var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;  
    // Match the date format through regular expression  
    if(inputText1.value.match(dateformat) && inputText2.value.match(dateformat))  
    {  
      document.form1.Check_in.focus();  
      //Test which seperator is used '/' or '-'  
      var opera1 = inputText1.value.split('/');  
      var opera2 = inputText1.value.split('-');  
      lopera1 = opera1.length;  
      lopera2 = opera2.length;  
      // Extract the string into month, date and year  
      if (lopera1>1)  
      {  
        var pdate = inputText1.value.split('/');  
      }  
      else if (lopera2>1)  
      {  
        var pdate = inputText1.value.split('-');  
      }  
      var dd = parseInt(pdate[0]);  
      var mm  = parseInt(pdate[1]);  
      var yy = parseInt(pdate[2]);  
      // Create list of days of a month [assume there is no leap year by default]  
      var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];  
      if (mm==1 || mm>2)  
      {  
        if (dd>ListofDays[mm-1])  
        {  
          //alert('Invalid date format0!');
          $(".check_in input").val('');
          $(".check_out input").val(''); 
          return false;  
        }  
      }  
      if (mm==2)  
      {  
        var lyear = false;  
        if ( (!(yy % 4) && yy % 100) || !(yy % 400))   
        {  
          lyear = true;  
        }  
        if ((lyear==false) && (dd>=29))  
        {  
          //alert('Invalid date format1!');
          $(".check_in input").val('');
          $(".check_out input").val('');
          return false;  
        }  
        if ((lyear==true) && (dd>29))  
        {  
          //alert('Invalid date format2!');
          $(".check_in input").val('');
          $(".check_out input").val(''); 
          return false;  
        }  
      }  
    }  
    else  
    {  
      //alert("Invalid date format3!");
      $(".check_in input").val('');
      $(".check_out input").val('');
      document.form1.Check_in.focus();  
      return false;  
    }
  }


  /*HOTEL FORM VALIDATION*/

$('.button-full-width-yellow').on('click', function () {
    var current = $(this).data('currentBlock'),
        next = $(this).data('nextBlock');
    if (next > current)
        if (false === $('#property-validation').parsley().validate('block' + current))
            return;

    // validation was ok. We can go on next step.
    $('#page' + current)
        .removeClass('active')

    $('#page' + next)
        .addClass('active');

    $('#tab' + current)
        .removeClass('active')

    $('#tab' + next)
        .addClass('active');

});


$('.hotel-create-back, #page1 .hotel-create-continue, #page2 .hotel-create-continue, #page3 .hotel-create-continue').click(function () {
	window.scrollTo(0, 0);
});