module PropertiesHelper
	def select_room(room)
		if session[:cart_id]
			cart = Cart.find(session[:cart_id])
			cart_item = cart.cart_items.where(:item_id => room.id).first
			if cart_item.nil?
				Array(1..room.count)
			else
				no_room = room.count - cart_item.quantity
				if no_room.eql?(0)
					Array(0)
				else
					Array(1..no_room)
				end
			end
		else
			Array(1..room.count)
		end

	end
end
