module Admin::CheckoutsHelper
	def search_field(field)
		params[:search] ? params[:search][field] : ""
	end
end
