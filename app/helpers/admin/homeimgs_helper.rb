module Admin::HomeimgsHelper
  def active_image(img, type)
    if type.eql?("class")
      if img.active?
        "btn-success"
      else
        "btn-danger"
      end
    else
      if img.active?
        "Active"
      else
        "Nonactive"
      end
    end
  end
end
