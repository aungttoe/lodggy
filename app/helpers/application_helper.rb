module ApplicationHelper
  def static_map_for(lat, lng, name, options = {})
    params = {
      :center => [lat, lng].join(","),
      :zoom => 16,
      :size => "680x200",
      :markers => [lat, lng].join(","),
      :sensor => true
      }.merge(options)
 
    query_string =  params.map{|k,v| "#{k}=#{v}"}.join("&")
    image_tag "http://maps.googleapis.com/maps/api/staticmap?#{query_string}", :alt => name
  end

	def is_active?(controller)
    if params[:controller].eql?(controller)
      "active"
    end
	end

	def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
