module CheckoutsHelper
	def error_fill(field)
		session[:profile] ? session[:profile][field] : ""
	end

  def total_price(cart_item)
    cart_item[:night].to_f * cart_item[:no_room].to_f * cart_item[:price].to_f
  end

  def government_tax(cart)
    total_price(cart) * 10 / 100
  end

  def service_tax(cart)
    total_price(cart) * 5 / 100
  end

  def total_cart(cart)
    total_price(cart) + service_tax(cart) + government_tax(cart)
  end
end
