module Dashboard::UsersHelper
	def change_total_price(night, room, price)
		night * room * price
	end

	def change_government(night, room, price)
		change_total_price(night, room, price) * 10 / 100
	end

	def change_service(night, room, price)
		change_total_price(night, room, price) * 5 / 100
	end

	def change_total_cart(night, room, price)
		change_total_price(night, room, price) + change_government(night, room, price) + change_service(night, room, price)
	end

	def check_status_payment(status)
		if status.eql?("Not Confirm")
      "alert"
    elsif status.eql?("Refunding") || status.eql?("Pending")
    	"secondary"
		else
    	"success"
    end
	end

	def check_published(status)
		unless status
			class_name = "alert"
			text			 = "Not Published"
		else
			class_name = "success"
			text			 = "Published"
		end
		"<label class='label #{class_name}'>#{text}</label>".html_safe
	end

	def check_deals(status)
		unless status
			class_name = "alert"
			text			 = "Not Deals"
		else
			class_name = "success"
			text			 = "Deals"
		end
		"<label class='label #{class_name}'>#{text}</label>".html_safe
	end
end
