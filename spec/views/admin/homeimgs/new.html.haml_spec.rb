require 'rails_helper'

RSpec.describe "admin/homeimgs/new", :type => :view do
  before(:each) do
    assign(:admin_homeimg, Admin::Homeimg.new())
  end

  it "renders new admin_homeimg form" do
    render

    assert_select "form[action=?][method=?]", admin_homeimgs_path, "post" do
    end
  end
end
