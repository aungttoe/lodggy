require 'rails_helper'

RSpec.describe "admin/homeimgs/edit", :type => :view do
  before(:each) do
    @admin_homeimg = assign(:admin_homeimg, Admin::Homeimg.create!())
  end

  it "renders the edit admin_homeimg form" do
    render

    assert_select "form[action=?][method=?]", admin_homeimg_path(@admin_homeimg), "post" do
    end
  end
end
