require "rails_helper"

RSpec.describe Admin::HomeimgsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/homeimgs").to route_to("admin/homeimgs#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/homeimgs/new").to route_to("admin/homeimgs#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/homeimgs/1").to route_to("admin/homeimgs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/homeimgs/1/edit").to route_to("admin/homeimgs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/homeimgs").to route_to("admin/homeimgs#create")
    end

    it "routes to #update" do
      expect(:put => "/admin/homeimgs/1").to route_to("admin/homeimgs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/homeimgs/1").to route_to("admin/homeimgs#destroy", :id => "1")
    end

  end
end
