class RemoveColumnFromCheckouts < ActiveRecord::Migration
  def change
  	Checkout.destroy_all
  	remove_column :checkouts, :hotel_id
  	remove_column :checkouts, :city
  	remove_column :checkouts, :rooms

  	add_column :checkouts, :property_id, :integer
  end
  
  def down
  	add_column :checkouts, :hotel_id, :integer
  	add_column :checkouts, :city, :string
  	add_column :checkouts, :rooms, :string

  	remove_column :checkouts, :property_id, :integer
  end
end
