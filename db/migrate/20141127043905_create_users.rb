class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      # User Detail
      t.string :first_name
      t.string :last_name
      t.string :special
      t.string :gender
      t.string :mobile
      t.string :country
      t.string :passport
      t.date :birth
      t.string :estimated
      t.string :special

      ## Credit Card
      t.string :card_number
      t.string :card_holders
      t.string :card_type
      t.string :cvv_code
      t.string :mounth
      t.integer :year

      # User Address
      t.string :select_country
      t.string :address
      t.string :state
      t.string :city
      t.string :postal_code

      #devise
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""
      t.string :reset_password_token
      t.datetime :reset_password_sent_at
      t.datetime :remember_created_at
      t.integer :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string :current_sign_in_ip
      t.string :last_sign_in_ip

      # omniauth
      t.string :provider
      t.string :uid
      #role
      t.string :role
      
      t.timestamps
    end
    add_index :users, :email,                unique: true
    add_index :users, :reset_password_token, unique: true
  end
end
