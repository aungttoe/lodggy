class AddCommentsCountToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :comments_count, :integer, :default => 0

    Property.reset_column_information
    Property.all.each do |property|
      property.update(:comments_count => property.comments.count)
    end
  end

  def down
    remove_column :properties, :comments_count
  end
end
