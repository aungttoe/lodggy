class AddCancellationToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :cancellation, :string, :default => 'Flexible'
    Room.all.each do |room|
    	room.cancellation = "Flexible"
    	room.save
    end
  end
end
