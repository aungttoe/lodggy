class CreateAbouts < ActiveRecord::Migration
  def change
    create_table :abouts do |t|
      t.string :column_1
      t.string :column_2
      t.string :column_3
      t.timestamps
    end
  end
end
