class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name
      t.string :resource_type
      t.string :resource_id

      t.timestamps
    end
  end
end
