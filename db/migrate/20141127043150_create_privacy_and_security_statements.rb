class CreatePrivacyAndSecurityStatements < ActiveRecord::Migration
  def change
    create_table :privacy_and_security_statements do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
