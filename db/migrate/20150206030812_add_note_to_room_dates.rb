class AddNoteToRoomDates < ActiveRecord::Migration
  def change
    add_column :room_dates, :note, :text
  end
end
