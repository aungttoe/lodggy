class AddCartIdToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :cart_id, :integer
  end
end
