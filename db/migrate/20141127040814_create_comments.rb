class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :name
      t.text :content
      t.integer :hotel_id
      t.integer :user_id
      t.string :user_email
      t.float :atmosphere
      t.float :staff
      t.float :location
      t.float :value_for_money
      t.float :security
      t.float :cleanliness
      t.float :facilities

      t.float :atmosphere_percent, default: 0
      t.float :staff_percent, default: 0
      t.float :location_percent, default: 0
      t.float :value_for_money_percent, default: 0
      t.float :security_percent, default: 0
      t.float :cleanliness_percent, default: 0
      t.float :facilities_percent, default: 0
      t.integer :general_rating, default: 0

      t.timestamps
    end
  end
end
