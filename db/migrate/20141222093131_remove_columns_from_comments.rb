class RemoveColumnsFromComments < ActiveRecord::Migration
  def change
  	remove_column :comments, :atmosphere_percent
  	remove_column :comments, :cleanliness_percent
  	remove_column :comments, :facilities_percent
  	remove_column :comments, :location_percent
  	remove_column :comments, :security_percent
  	remove_column :comments, :staff_percent
  	remove_column :comments, :value_for_money_percent
  	remove_column :comments, :name
  	remove_column :comments, :user_email
  end
end
