class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.string :title
      t.string :address
      t.text :description
      t.string :property_type
      t.text :content
      t.text :location
      t.text :directions
      t.text :things
      t.string :state
      t.string :address_two
      t.string :postal_code
      t.integer :bedrooms
      t.integer :price
      t.string :contact_name
      t.string :email
      t.string :website
      t.string :phone
      t.string :currency
      t.string :language
      t.references :city, index: true

      t.timestamps
    end
  end
end
