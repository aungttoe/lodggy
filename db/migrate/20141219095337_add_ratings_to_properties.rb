class AddRatingsToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :atmosphere_rating, :integer, default: 0
    add_column :properties, :staff_rating, :integer, default: 0
    add_column :properties, :security_rating, :integer, default: 0
    add_column :properties, :cleanliness_rating, :integer, default: 0
    add_column :properties, :location_rating, :integer, default: 0
    add_column :properties, :facilities_rating, :integer, default: 0
    add_column :properties, :value_of_money_rating, :integer, default: 0

  end
end
