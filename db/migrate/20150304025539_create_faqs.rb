class CreateFaqs < ActiveRecord::Migration
  def change
    create_table :faqs do |t|
      t.string :name
      t.text :content
      t.integer :parent_id

      t.timestamps
    end
  end
end
