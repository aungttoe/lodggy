class AddCancellationToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :cancellation, :string
    Property.all.each do |property|
    	property.cancellation = "Flexible"
    	property.save
    end
  end
end
