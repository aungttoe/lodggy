class AddCityToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :city, :string
  end
end
