class AddRoomTypeToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :room_type, :string
  end
end
