class ModifyDetailBookings < ActiveRecord::Migration
  def change
  	remove_column :bookings, :email
  	remove_column :bookings, :first_name
  	remove_column :bookings, :last_name

  	add_column :bookings, :no_room, :integer, default: 0
  	add_column :bookings, :checkout_id, :integer
  end
end
