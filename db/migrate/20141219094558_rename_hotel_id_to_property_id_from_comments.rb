class RenameHotelIdToPropertyIdFromComments < ActiveRecord::Migration
  def change
  	Comment.destroy_all
  	
  	rename_column :comments, :hotel_id, :property_id
  end
end
