class CreateUserRefunds < ActiveRecord::Migration
  def change
    create_table :user_refunds do |t|
      t.integer :amount
      t.integer :checkout_id
      t.date :confirm_at

      t.timestamps
    end
  end
end
