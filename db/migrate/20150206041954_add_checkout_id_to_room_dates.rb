class AddCheckoutIdToRoomDates < ActiveRecord::Migration
  def change
    add_column :room_dates, :checkout_id, :integer
    add_index :room_dates, :checkout_id
  end
end
