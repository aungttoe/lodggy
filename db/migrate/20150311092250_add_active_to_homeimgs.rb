class AddActiveToHomeimgs < ActiveRecord::Migration
  def change
    add_column :homeimgs, :active, :boolean, :default => false
  end
end
