class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string :title
      t.text :content
      t.integer :private_room_price
      t.integer :dorm_room_from
      t.float :distance_from_city_center
      t.float :rating_percent
      t.float :rating
      t.string :url
      t.boolean :deals, default: false
      t.boolean :published, default: false
      t.integer :room_id
      t.integer :comment_id
      t.string :address_two
      t.string :property_type
      t.string :total_number
      t.integer :number_of_beds
      t.string :postcode
      t.string :contact_name
      t.string :manager_email
      t.string :bookings_email
      t.string :website_address
      t.string :currency
      t.string :language
      t.string :how
      t.string :phone_number
      t.string :website
      t.text :description
      t.text :location
      t.text :directions
      t.text :things
      t.integer :city_id
      t.integer :user_id
      t.integer :general_rating, default: 0
      t.boolean :hour_reception, default: false
      t.boolean :card_phones, default: false
      t.boolean :gum, default: false
      t.boolean :hour_security, default: false
      t.boolean :nightclub, default: false
      t.boolean :celing_fan, default: false
      t.boolean :hairdryers, default: false
      t.boolean :outdoor_swimming, default: false
      t.boolean :adaptors, default: false
      t.boolean :cots_available, default: false
      t.boolean :hot_tub, default: false
      t.boolean :outdoor_terrace, default: false
      t.boolean :air_conditioning, default: false
      t.string :currency_exchange
      t.boolean :housekeeping, default: false
      t.boolean :parking, default: false
      t.boolean :airport_transfers, default: false
      t.boolean :direct_dial, default: false
      t.boolean :indoor_swimming, default: false
      t.boolean :postal_service, default: false
      t.boolean :atm, default: false
      t.boolean :dvds, default: false
      t.boolean :internet_access, default: false
      t.boolean :reading_light, default: false
      t.boolean :bar, default: false
      t.boolean :elevator, default: false
      t.boolean :key_card, default: false
      t.boolean :restaurant, default: false
      t.boolean :bicucle_hire, default: false
      t.boolean :fax_service, default: false
      t.boolean :landry_facilities, default: false
      t.boolean :safe_deposit, default: false
      t.boolean :book_exchenge, default: false
      t.boolean :free_airport, default: false
      t.boolean :lounge, default: false
      t.boolean :shuttle_bus, default: false
      t.boolean :breakfast_include, default: false
      t.boolean :free_city, default: false
      t.boolean :luggage_storage, default: false
      t.boolean :swimming_pool, default: false
      t.boolean :breakfast_not_include, default: false
      t.boolean :free_internet, default: false
      t.boolean :meeting_room, default: false
      t.boolean :making_facilities, default: false
      t.boolean :business_centre, default: false
      t.boolean :free_parking, default: false
      t.boolean :mini_bar, default: false
      t.boolean :cable_tv, default: false
      t.boolean :free_wifi, default: false
      t.boolean :mini_supermarket, default: false
      t.boolean :vending_mashines, default: false
      t.boolean :wifi, default: false
      t.boolean :age_restriction, default: false
      t.boolean :credit_card_not, default: false
      t.boolean :no_curfew, default: false
      t.boolean :taxes_included, default: false
      t.boolean :child_friendly, default: false
      t.boolean :curfew, default: false
      t.boolean :non_smoking, default: false
      t.boolean :taxes_not_included, default: false
      t.boolean :credit_cards_acc, default: false
      t.boolean :lock_out, default: false
      t.boolean :pet_friendly, default: false
      t.boolean :all_property, default: false
      t.boolean :hostels, default: false
      t.boolean :bed_and_breakfasts, default: false
      t.boolean :hotels, default: false
      t.boolean :ensuite_room, default: false
      t.boolean :single_room, default: false
      t.boolean :twin_room, default: false
      t.boolean :double_room, default: false
      t.boolean :triple_room, default: false
      t.boolean :family_room, default: false
      t.boolean :mixed_dorm, default: false
      t.boolean :male_dorm, default: false
      t.boolean :female_dorm, default: false
      t.boolean :breakfast_included, default: false
      t.boolean :internet_access, default: false
      t.string :luggage_storage
      t.boolean :hour_check_in, default: false
      t.boolean :restaurant, default: false
      t.boolean :bar, default: false
      t.boolean :air_conditioning, default: false
      t.boolean :common_room, default: false
      t.boolean :travel_desk, default: false
      t.boolean :wheelchair_friendly, default: false
      t.boolean :all_payment, default: false
      t.boolean :credit_card, default: false
      t.boolean :debit_card, default: false
      t.boolean :paypal, default: false
      t.boolean :special_payment, default: false

      t.integer :atmosphere_percent, default: 0
      t.integer :staff_percent, default: 0
      t.integer :location_percent, default: 0
      t.integer :value_for_money_percent, default: 0
      t.integer :security_percent, default: 0
      t.integer :cleanliness_percent, default: 0
      t.integer :facilities_percent, default: 0

      t.string :address
      t.string :town
      t.string :state
      t.string :country
      t.float :lat
      t.float :lng
      t.string :coordinates

      t.timestamps
    end
  end
end
