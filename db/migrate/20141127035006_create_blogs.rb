class CreateBlogs < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :body
      t.integer :blog_category_id
      t.integer :city_id
      t.string :image

      t.timestamps
    end
  end
end
