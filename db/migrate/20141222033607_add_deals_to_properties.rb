class AddDealsToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :deals, :boolean, default: false
  end
end
