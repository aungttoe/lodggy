class AddRoomIdToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :room_id, :integer
    add_index :checkouts, :room_id
  end
end
