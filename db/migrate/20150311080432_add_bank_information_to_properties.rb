class AddBankInformationToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :account_number, :string
    add_column :properties, :account_name, :string
    add_column :properties, :bank_name, :string
    add_column :properties, :swift, :string
    add_column :properties, :bank_address, :text
  end
end
