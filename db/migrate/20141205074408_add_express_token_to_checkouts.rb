class AddExpressTokenToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :express_token, :string
    add_column :checkouts, :express_payer_id, :string
  end
end
