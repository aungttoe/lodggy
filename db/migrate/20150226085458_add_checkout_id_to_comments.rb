class AddCheckoutIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :checkout_id, :integer

    add_index :comments, :checkout_id
  end
end
