class RemoveCurrencyAndLanguageFromProperties < ActiveRecord::Migration
  def change
  	remove_column :properties, :currency
  	remove_column :properties, :language
  end
end
