class AddCreditCardToCheckouts < ActiveRecord::Migration
  def change
  	add_column :checkouts, :card_number, :string
  	add_column :checkouts, :brand, :string
  	add_column :checkouts, :year, :integer
  	add_column :checkouts, :month, :integer
  end
end
