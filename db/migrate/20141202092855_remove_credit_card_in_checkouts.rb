class RemoveCreditCardInCheckouts < ActiveRecord::Migration
  def change
  	remove_column :checkouts, :card_number
  	remove_column :checkouts, :card_holders
  	remove_column :checkouts, :card_type
  	remove_column :checkouts, :cvv_code
  	remove_column :checkouts, :mounth
  	remove_column :checkouts, :year
  end
end
