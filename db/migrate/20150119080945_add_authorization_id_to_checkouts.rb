class AddAuthorizationIdToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :authorization_id, :string
  end
end
