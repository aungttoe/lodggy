class AddPriceToRoomDates < ActiveRecord::Migration
  def change
    add_column :room_dates, :price, :integer
  end
end
