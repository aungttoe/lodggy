class CreateRoomDates < ActiveRecord::Migration
  def change
    create_table :room_dates do |t|
      t.date :from
      t.date :to
      t.string :type_date
      t.integer :no_room
      t.integer :room_id

      t.timestamps
    end

    add_index :room_dates, :room_id
  end
end
