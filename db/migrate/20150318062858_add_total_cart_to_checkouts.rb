class AddTotalCartToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :total_cart, :decimal, :precision => 8, :scale => 2, :default => 0
  end
end
