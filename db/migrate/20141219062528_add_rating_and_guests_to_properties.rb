class AddRatingAndGuestsToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :rating, :integer, default: 0
    add_column :properties, :guests, :integer, default: 0
  end
end
