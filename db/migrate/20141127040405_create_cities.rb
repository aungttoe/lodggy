class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.integer :hotel_id
      t.text :description
      t.string :user_email
      t.integer :min_price
      t.integer :total
      t.string :density
      t.string :ethnicities
      t.string :religions
      t.text   :content_city
      t.string :cityimg
      t.string :address
      t.string :town
      t.string :state
      t.string :country
      t.float :lat
      t.float :lng
      t.string :coordinates
      
      t.timestamps
    end
  end
end
