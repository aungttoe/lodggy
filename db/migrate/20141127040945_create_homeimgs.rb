class CreateHomeimgs < ActiveRecord::Migration
  def change
    create_table :homeimgs do |t|
      t.string :title
      t.string :image

      t.timestamps
    end
  end
end
