class AddInstantToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :instant, :boolean, default: false
  end
end
