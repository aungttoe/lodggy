class ChangeCurrencyIntegerToDecimal < ActiveRecord::Migration
  def change
    change_column :rooms, :price, :decimal, :precision => 8, :scale => 2
    change_column :properties, :price, :decimal, :precision => 8, :scale => 2
    change_column :room_dates, :price, :decimal, :precision => 8, :scale => 2
  end
end
