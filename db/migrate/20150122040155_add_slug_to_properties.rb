class AddSlugToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :slug, :string
    add_index :properties, :slug, unique: true
    Property.find_each(&:save)
  end
end
