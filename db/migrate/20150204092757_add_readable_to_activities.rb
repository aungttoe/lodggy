class AddReadableToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :is_read_admin, :boolean, :default => false
    add_column :activities, :is_read_hotel, :boolean, :default => false
  end
end
