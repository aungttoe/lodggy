class ChangeColumnRoomTypeToRoomsFromCheckouts < ActiveRecord::Migration
  def change
  	rename_column :checkouts, :room_type, :rooms
  end
end
