class CreateFaqPosts < ActiveRecord::Migration
  def change
    create_table :faq_posts do |t|
      t.string :title
      t.text :content
      t.references :faq_category

      t.timestamps
    end
  end
end
