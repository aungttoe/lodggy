class ChangeAmountInUserRefunds < ActiveRecord::Migration
  def change
    change_column :user_refunds, :amount, :decimal, :precision => 8, :scale => 2, :default => 0
  end
end
