class AddBookingIdToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :booking_id, :integer
    add_index :checkouts, :booking_id, unique: true
  end
end
