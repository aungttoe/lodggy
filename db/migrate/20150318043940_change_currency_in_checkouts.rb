class ChangeCurrencyInCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :total_price, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :checkouts, :service, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :checkouts, :government, :decimal, :precision => 8, :scale => 2, :default => 0
    add_column :checkouts, :per_night, :decimal, :precision => 8, :scale => 2, :default => 0

    Checkout.destroy_all

    remove_column :checkouts, :general_price
  end
end
