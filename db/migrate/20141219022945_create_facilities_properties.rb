class CreateFacilitiesProperties < ActiveRecord::Migration
  def change
    create_table :facilities_properties, id: false do |t|
    	t.belongs_to :facility
    	t.belongs_to	:property
    end
  end
end
