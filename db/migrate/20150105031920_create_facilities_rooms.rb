class CreateFacilitiesRooms < ActiveRecord::Migration
  def change
    create_table :facilities_rooms, :id => false do |t|
			t.references :facility
			t.references :room
    end

    add_index :facilities_rooms, [:facility_id, :room_id]
  end
end
