class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :title
      t.text :about
      t.integer :price
      t.date :check_in_room
      t.date :check_out_room
      t.integer :hotel_id
      t.integer :booking_id
      t.references :room_type
      t.string :hotel_name
      t.integer :count
      t.string :city
      t.integer :guest
      t.string :pay_type
      t.boolean :ensuite, default: false


      t.timestamps
    end
  end
end
