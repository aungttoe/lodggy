class AddStatusToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :status, :string
  end
end
