class AddDetailsToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :check_in, :date
    add_column :bookings, :check_out, :date
    add_column :bookings, :guest, :integer
    add_column :bookings, :night, :integer
    add_column :bookings, :price, :integer
  end
end
