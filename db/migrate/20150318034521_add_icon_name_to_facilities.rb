class AddIconNameToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :icon_name, :string
  end
end
