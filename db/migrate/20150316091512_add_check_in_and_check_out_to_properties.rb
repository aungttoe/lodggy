class AddCheckInAndCheckOutToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :check_in, :integer, default: 0
    add_column :properties, :check_out, :integer, default: 0
  end
end
