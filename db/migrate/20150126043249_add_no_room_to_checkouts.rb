class AddNoRoomToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :no_room, :integer
  end
end
