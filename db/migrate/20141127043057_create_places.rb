class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :title
      t.text :content
      t.integer :city_id
      t.string :placeimg
      t.string :address
      t.string :town
      t.string :state
      t.string :country
      t.float :lat
      t.float :lng
      t.string :coordinates

      t.timestamps
    end
  end
end
