class AddReviewToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :review, :boolean, :default => false
  end
end
