class AddReferenceIdToCheckouts < ActiveRecord::Migration
  def change
    add_column :checkouts, :reference_id, :string
    add_index :checkouts, :reference_id, unique: true

    Checkout.all.each do |checkout|
    	checkout.reference_id = SecureRandom.base64(8).tr("+/=", "Zab")
    	checkout.save
    end
  end
end
