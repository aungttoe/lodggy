class CreateCheckouts < ActiveRecord::Migration
  def change
    create_table :checkouts do |t|
      t.string :last_name
      t.string :first_name
      t.string :email_id
      t.string :special
      t.string :gender
      t.string :mobile
      t.string :country
      t.string :passport
      t.date :birth
      t.string :estimate
      t.integer :card_number
      t.string :card_holders
      t.string :card_type
      t.string :cvv_code
      t.string :mounth
      t.string :year
      t.string :select_country
      t.string :address
      t.string :state
      t.string :city
      t.string :postal_code
      t.string :room_type
      t.string :general_price
      t.integer :nights
      t.date :check_in
      t.date :check_out
      t.string :guest
      t.string :pay_type
      # t.string :type
      t.integer :hotel_id
      t.integer :user_id
      t.string :payment
      t.string :custom_id
      t.timestamps
    end
  end
end
