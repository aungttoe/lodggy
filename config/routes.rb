Mmlodging::Application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'


  #admin routes
  namespace :admin do
    root "dashboard#index"
    get "search" => "dashboard#search", as: :search
    get "notifications" => "activities#index", as: :activities
    get "places" => "places#index", as: :all_places
    get "hotels" => "hotels#index", as: :all_hotels
    get "properties" => "properties#index", as: :all_properties
    get "refunds" => "user_refunds#index", as: :all_refunds
    post "confirm_refund" => "user_refunds#confirm_refund", as: :confirm_refund
    #checkouts
    get "checkouts" => "checkouts#index", as: :all_checkouts
    post "checkouts/confirm/:id" => "checkouts#confirm", as: :confirm_checkout

    post "new_facility" => "properties#new_facility", as: :new_facility

    post "page/update" => 'page#update', as: :update_page
    get "page/cancellation" => "page#cancellation", as: :cancellation_page
    get "page/terms" => "page#terms", as: :terms_page
    get "page/story" => "page#story", as: :story_page
    get "page/privacy" => "page#privacy", as: :privacy_page
    get "page/homepage" => "page#homepage", as: :homepage

    resources :faqs do
      member do
        get "child"
        post "child" => "faqs#create_child"
        get "new_child"
      end
    end

    resources :checkouts do
      get "detail"
      collection do
        get "user/:user_id" => "checkouts#user", as: :user
      end
    end

    resources :homeimgs do
      member do
        patch "active"
      end
    end

    resources :blogs do
      member do
        delete "destroy_comment/:comment_id" => "blogs#destroy_comment", as: :destroy_comment
      end
    end
    resources :users
    resources :cities do
      resources :properties do
        post "published"
        post "deals"
        resources :rooms do
          resources :pricings do
            collection do
              get "no_room" => 'pricings#no_room', as: :no_room
            end
          end
        end
      end
      resources :places
    end
  end
  #dashboard user routes
  namespace :dashboard do
    root "users#index"
    get "checkouts/:property_id" => "users#checkouts", :as => :checkouts
    get "checkouts/:property_id/detail/:id" => "users#checkout_detail", :as => :checkout_detail
    post "checkouts/cancel/:id" => "users#cancel", :as => :cancel
    get "checkouts/detail/:id" => "users#details", :as => :details
    get "checkouts/change_date/:id" => "users#change", :as => :change_date
    post "checkouts/change_date/:id" => "users#change_date", :as => :change_date_update
    get "checkouts/detail/:id/cancel" => "users#confirm_cancel", :as => :confirm_cancel
    get "checkouts/check_available/:id" => "users#check_available", :as => :check_available
    post "checkouts/payment/:id" => "users#payment", :as => :payment
    resources :change_bookings
  end

  #page static
  get "privacy" => "homepages#privacy", :as => :privacy
  get "story" => "homepages#story", :as => :story
  get "terms" => "homepages#terms", :as => :terms
  get "cancellation" => "homepages#cancellation", :as => :cancellation
  get "faq" => "homepages#faq", :as => :faq

  #properties and review
  resources :properties, only: :show do
    get "map"
    get "reviews"
    resources :comments, only: [:create, :destroy]
  end

  #cities
  get "cities_info" => "cities#cities_info", :as => :cities_info
  resources :cities, :only => :show do
    resources :places, :only => [:index, :show]
    member do
      get "map"
    end
  end

  #blogs routes
  get "blog" => "blogs#index", :as => :blogs
  get "blog/:id" => "blogs#show", :as => :blog
  post "blog/:id/comment" => "blog_comments#create", :as => :blog_comment
  get "blog/tags/:tag" => "blogs#tags", :as => :tags_blog

  # Checkouts Controller
  post "/checkouts" => "checkouts#create", :as => :checkouts
  get "/checkouts/success" => "checkouts#success", :as => :checkouts_success
  get "/checkouts/express" => "checkouts#express", :as => :checkouts_express
  get "/checkouts/new" => "checkouts#new", :as => :new_checkout
  post "/checkouts/new" => "checkouts#new"

  #search
  get '/map_search', to: 'search#map_search'
  get "search" => "search#index", :as => :search_index

  #to mailer contact
  get "contacts/new"
  post "/contacts", to: "contacts#create"

  
  devise_for :users,
    :controllers => {
      :registrations => "registrations",
      :omniauth_callbacks => 'omniauth',
      :sessions => 'sessions',
      :confirmations => 'confirmations'
    }

  root 'cities#index'
  match "*path", to: "application#record_not_found", via: :all  
end
