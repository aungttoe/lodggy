require "rvm/capistrano"
# require "bundler/capistrano"

set :application, "mmlodging"
set :domain, "developer@dev.mmlodging.com"
set :deploy_to, "/var/www/#{application}"
set :use_sudo, false
set :unicorn_conf, "#{deploy_to}/current/config/unicorn.rb"
set :unicorn_pid, "#{deploy_to}/shared/pids/unicorn.pid"

set :rvm_ruby_string, 'ruby-2.1-head'

set :scm, :git
set :repository,  "git@gitlab.com:41studio/mmlodging.git"
set :branch, "master"
set :deploy_via, :remote_cache

role :web, domain
role :app, domain
role :db,  domain, :primary => true

set :keep_releases, 5

set :auth_method, ["public_key"]

after "deploy", "deploy:cleanup" # keep only the last 5 releases

set :shared_children, shared_children + %w{public/uploads public/assets}

set :normalize_asset_timestamps, false

set :rails_env, "production"

namespace :rake do
  namespace :db do
    %w[create migrate reset rollback seed setup].each do |command|
      desc "Rake db:#{command}"
      task command, roles: :app, except: {no_release: true} do
        run "cd #{deploy_to}/current; bundle exec rake db:#{command} RAILS_ENV=#{rails_env}"
      end
    end
  end

  namespace :assets do
    %w[precompile clean].each do |command|
      desc "Rake assets:#{command}"
      task command, roles: :app, except: {no_release: true} do
        run "cd #{deploy_to}/current; bundle exec rake assets:#{command} RAILS_ENV=#{rails_env}"
      end
    end
  end
end

namespace :deploy do
  task :prepare_app, roles: :app do
    run "ln -nfs #{shared_path}/public/uploads/ #{release_path}/public/uploads"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "chmod +x #{release_path}/config/unicorn_init.sh"
    sudo "ln -nfs #{release_path}/config/unicorn_init.sh /etc/init.d/unicorn_mmlodging"
    run "ln -nfs #{shared_path}/public/assets/ #{release_path}/public/assets"
    run "cd #{release_path}; bundle install"
    run "cd #{release_path}; bundle exec rake db:migrate RAILS_ENV=production"
    run "cd #{release_path}; bundle exec rake assets:precompile RAILS_ENV=production"
  end

  task :restart do
    sudo "service unicorn_mmlodging restart"
    sudo "service nginx restart"
  end

  task :start do
    sudo "service unicorn_mmlodging start"
    sudo "service nginx start"
  end

  task :stop do
    sudo "service unicorn_mmlodging stop"
    sudo "service nginx stop"
  end

  after "deploy:finalize_update", "deploy:prepare_app"
end
