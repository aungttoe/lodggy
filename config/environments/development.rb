Mmlodging::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  ## config.action_mailer.raise_delivery_errors = false
  config.action_view.embed_authenticity_token_in_remote_forms = true
  # ActionMailer Config
  config.action_mailer.default_url_options = { :host => 'lvh.me:3000' }
  config.action_controller.asset_host = 'lvh.me:3000'
  config.action_mailer.asset_host = 'lvh.me:3000'
  # A dummy setup for development - no deliveries, but logged
  
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = { :address => "localhost", :port => 1025 }
  config.action_mailer.perform_deliveries = true

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default :charset => "utf-8"

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  ActiveMerchant::Billing::Base.mode = :test
  paypal_options = {
    :login => "Dimas-facilitator_api1.41studio.com",
    :password => "1401852084",
    :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31AbGfHwlbLJ5SLQthiBgDCj8AE7.Y"
  }
  ::STANDARD_GATEWAY = ActiveMerchant::Billing::PaypalGateway.new(paypal_options)
  ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressGateway.new(paypal_options)
end
