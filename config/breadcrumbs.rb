crumb :root do
  link "Dashboard", admin_root_path
end

# Cities

crumb :cities do
  link "All Cities", admin_cities_path
end

crumb :city do |city|
  link city.name, admin_city_path(city)
  parent :cities
end

crumb :new_city do
  link "New City", new_admin_city_path
  parent :cities
end

crumb :edit_city do |city|
  link "Edit", edit_admin_city_path(city)
  parent :city, city
end

# Properties

crumb :properties do
  link "All Properties", admin_all_properties_path
end

crumb :property do |property|
  link property.title, edit_admin_city_property_path(property.city, property)
  parent :root
end

crumb :new_property do |city|
  link "New Property", new_admin_city_property_path(city)
  parent :city, city
end

crumb :edit_property do |property|
  link "Edit", edit_admin_city_property_path(property.city, property)
  parent :property, property
end

# Rooms

crumb :rooms do |property|
  link "All Rooms", admin_city_property_rooms_path
  parent :property, property
end

crumb :room do |room|
  link room.title, admin_city_property_rooms_path
  parent :property, room.property
end

crumb :edit_room do |room|
  link "Edit", ""
  parent :room, room
end

crumb :new_room do |property|
  link "New", ""
  parent :property, property
end

crumb :manage_date_pricing do |room|
  link "Manage Date and Pricing", ""
  parent :room, room
end
# Checkouts

crumb :checkouts do
  link "All Checkouts", admin_checkouts_path
end

crumb :checkout do |checkout|
  link checkout.id, admin_checkout_detail_path(checkout)
  parent :checkouts
end

# Refunds

crumb :cancellation do
  link "All Cancellation", admin_all_refunds_path
end

# Users

crumb :users do
  link "All Users", admin_users_path
end

crumb :user do |user|
  link user.email, admin_user_path(user)
  parent :root
end

crumb :edit_user do |user|
  link "Edit", edit_admin_user_path(user)
  parent :user, user
end

crumb :new_user do
  link "New User", new_admin_user_path
  parent :root
end

# Places

crumb :places do
  link "All Places", admin_all_places_path
end

crumb :place do |place|
  link place.title, city_place_path(place.city, place)
  parent :root
end

crumb :edit_place do |place|
  link "Edit", edit_admin_city_place_path(place.city, place)
  parent :place, place
end

crumb :new_place do |city|
  link "New Place", new_admin_city_place_path
  parent :city, city
end

# Page

crumb :cancellation do
  link "Cancellation", admin_cancellation_page_path
end

crumb :terms do
  link "Terms and Conditions", admin_terms_page_path
end

crumb :privacy do
  link "Privacy Policy", admin_privacy_page_path
end

crumb :story do
  link "Our Story", admin_story_page_path
end

# FAQ

crumb :faqs do
  link "All Categories FAQ", admin_faqs_path
end

crumb :faq do |faq|
  link faq.name, admin_faq_path(faq)
  parent :root
end

crumb :new_faq do |faq|
  link "New FAQ", new_child_admin_faq_path(faq)
  parent :faq, faq
end

crumb :view_faq do |faq|
  link faq.name, child_admin_faq_path(faq)
  parent :faq, Faq.find(faq.parent_id)
end

crumb :notifications do
  link "All Notifications", admin_activities_path
end

crumb :search do
  link "Search", admin_search_path
end

#homeimgs

crumb :homeimgs do
  link "All Images", admin_homeimgs_path
end

crumb :new_homeimg do
  link "New Image", new_admin_homeimg_path
  parent :homeimgs
end

crumb :edit_homeimg do |homeimg|
  link "Edit #{homeimg.title}", edit_admin_homeimg_path(homeimg)
  parent :homeimgs
end

# Blog
crumb :blogs do
  link "All Blogs", admin_blogs_path
end

crumb :blog do |blog|
  link blog.title, admin_blog_path(blog)
  parent :blogs
end

crumb :new_blog do
  link "New Blog", new_admin_blog_path
  parent :blogs
end

crumb :edit_blog do |blog|
  link "Edit #{blog.title}", edit_admin_blog_path(blog)
  parent :blog, blog
end