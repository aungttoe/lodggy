working_directory "/var/www/mmlodging/current"

pid "/var/www/mmlodging/shared/pids/unicorn.pid"

stderr_path "/var/www/mmlodging/shared/log/unicorn.log"
stdout_path "/var/www/mmlodging/shared/log/unicorn.log"

listen "/tmp/unicorn.mmlodging.sock"

worker_processes 2

timeout 30