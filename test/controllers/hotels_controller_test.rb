require 'test_helper'

class HotelsControllerTest < ActionController::TestCase
  setup do
    @hotel = hotels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:hotels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hotel" do
    assert_difference('Hotel.count') do
      post :create, hotel: { content: @hotel.content, distance_from_city_center: @hotel.distance_from_city_center, dorm_room_from: @hotel.dorm_room_from, private_room_price: @hotel.private_room_price, rating_percent: @hotel.rating_percent, title: @hotel.title }
    end

    assert_redirected_to hotel_path(assigns(:hotel))
  end

  test "should show hotel" do
    get :show, id: @hotel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hotel
    assert_response :success
  end

  test "should update hotel" do
    patch :update, id: @hotel, hotel: { content: @hotel.content, distance_from_city_center: @hotel.distance_from_city_center, dorm_room_from: @hotel.dorm_room_from, private_room_price: @hotel.private_room_price, rating_percent: @hotel.rating_percent, title: @hotel.title }
    assert_redirected_to hotel_path(assigns(:hotel))
  end

  test "should destroy hotel" do
    assert_difference('Hotel.count', -1) do
      delete :destroy, id: @hotel
    end

    assert_redirected_to hotels_path
  end
end
