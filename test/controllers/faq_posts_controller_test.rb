require 'test_helper'

class FaqPostsControllerTest < ActionController::TestCase
  setup do
    @faq_post = faq_posts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:faq_posts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create faq_post" do
    assert_difference('FaqPost.count') do
      post :create, faq_post: { content: @faq_post.content, title: @faq_post.title }
    end

    assert_redirected_to faq_post_path(assigns(:faq_post))
  end

  test "should show faq_post" do
    get :show, id: @faq_post
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @faq_post
    assert_response :success
  end

  test "should update faq_post" do
    patch :update, id: @faq_post, faq_post: { content: @faq_post.content, title: @faq_post.title }
    assert_redirected_to faq_post_path(assigns(:faq_post))
  end

  test "should destroy faq_post" do
    assert_difference('FaqPost.count', -1) do
      delete :destroy, id: @faq_post
    end

    assert_redirected_to faq_posts_path
  end
end
