require 'test_helper'

class HomeimgsControllerTest < ActionController::TestCase
  setup do
    @homeimg = homeimgs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:homeimgs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create homeimg" do
    assert_difference('Homeimg.count') do
      post :create, homeimg: { title: @homeimg.title }
    end

    assert_redirected_to homeimg_path(assigns(:homeimg))
  end

  test "should show homeimg" do
    get :show, id: @homeimg
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @homeimg
    assert_response :success
  end

  test "should update homeimg" do
    patch :update, id: @homeimg, homeimg: { title: @homeimg.title }
    assert_redirected_to homeimg_path(assigns(:homeimg))
  end

  test "should destroy homeimg" do
    assert_difference('Homeimg.count', -1) do
      delete :destroy, id: @homeimg
    end

    assert_redirected_to homeimgs_path
  end
end
