require 'test_helper'

class JsinsideControllerTest < ActionController::TestCase
  test "should get faq" do
    get :faq
    assert_response :success
  end

  test "should get security" do
    get :security
    assert_response :success
  end

end
