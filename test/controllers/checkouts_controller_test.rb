require 'test_helper'

class CheckoutsControllerTest < ActionController::TestCase
  setup do
    @checkout = checkouts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:checkouts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create checkout" do
    assert_difference('Checkout.count') do
      post :create, checkout: { address: @checkout.address, birth: @checkout.birth, card_holders: @checkout.card_holders, card_number: @checkout.card_number, card_type: @checkout.card_type, city: @checkout.city, country: @checkout.country, cvv_code: @checkout.cvv_code, email_id: @checkout.email_id, estimate: @checkout.estimate, first_name: @checkout.first_name, gender: @checkout.gender, last_name: @checkout.last_name, mobile: @checkout.mobile, mounth: @checkout.mounth, postal_code: @checkout.postal_code, select_country: @checkout.select_country, special: @checkout.special, state: @checkout.state, year: @checkout.year }
    end

    assert_redirected_to checkout_path(assigns(:checkout))
  end

  test "should show checkout" do
    get :show, id: @checkout
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @checkout
    assert_response :success
  end

  test "should update checkout" do
    patch :update, id: @checkout, checkout: { address: @checkout.address, birth: @checkout.birth, card_holders: @checkout.card_holders, card_number: @checkout.card_number, card_type: @checkout.card_type, city: @checkout.city, country: @checkout.country, cvv_code: @checkout.cvv_code, email_id: @checkout.email_id, estimate: @checkout.estimate, first_name: @checkout.first_name, gender: @checkout.gender, last_name: @checkout.last_name, mobile: @checkout.mobile, mounth: @checkout.mounth, postal_code: @checkout.postal_code, select_country: @checkout.select_country, special: @checkout.special, state: @checkout.state, year: @checkout.year }
    assert_redirected_to checkout_path(assigns(:checkout))
  end

  test "should destroy checkout" do
    assert_difference('Checkout.count', -1) do
      delete :destroy, id: @checkout
    end

    assert_redirected_to checkouts_path
  end
end
