require 'test_helper'

class PrivacyAndSecurityStatementsControllerTest < ActionController::TestCase
  setup do
    @privacy_and_security_statement = privacy_and_security_statements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:privacy_and_security_statements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create privacy_and_security_statement" do
    assert_difference('PrivacyAndSecurityStatement.count') do
      post :create, privacy_and_security_statement: { body: @privacy_and_security_statement.body, title: @privacy_and_security_statement.title }
    end

    assert_redirected_to privacy_and_security_statement_path(assigns(:privacy_and_security_statement))
  end

  test "should show privacy_and_security_statement" do
    get :show, id: @privacy_and_security_statement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @privacy_and_security_statement
    assert_response :success
  end

  test "should update privacy_and_security_statement" do
    patch :update, id: @privacy_and_security_statement, privacy_and_security_statement: { body: @privacy_and_security_statement.body, title: @privacy_and_security_statement.title }
    assert_redirected_to privacy_and_security_statement_path(assigns(:privacy_and_security_statement))
  end

  test "should destroy privacy_and_security_statement" do
    assert_difference('PrivacyAndSecurityStatement.count', -1) do
      delete :destroy, id: @privacy_and_security_statement
    end

    assert_redirected_to privacy_and_security_statements_path
  end
end
