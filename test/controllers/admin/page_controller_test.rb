require 'test_helper'

class Admin::PageControllerTest < ActionController::TestCase
  test "should get cancel" do
    get :cancel
    assert_response :success
  end

  test "should get terms" do
    get :terms
    assert_response :success
  end

end
